function mostrarPopUp() {    
    $("#pop").fadeIn('slow');
    $("#overlayPop").fadeIn('slow');    
}

function cerrarPopUp() {
    $("#pop").fadeOut('slow');
    $("#overlayPop").fadeOut('slow');
}

/*$(document).ready(function (){*/
jQuery().ready(function(){
    //Conseguir valores de la Tabla
    $("#pop").css('display', 'block');
    var table_w = $("#pop table").width() + 10;
    var table_h = $("#pop table").height() + 28;

    //Darle el alto y ancho
    $("#pop").css('width', table_w + 'px');
    $("#pop").css('height', table_h + 'px');

    //Esconder el popup
    $("#pop").hide();

    //Consigue valores de la ventana del navegador
    var w = $(this).width();
    var h = $(this).height();

    $("#overlayPop").css('width', w);
    $("#overlayPop").css('height', h-2);
    $("#overlayPop").css('-moz-opacity', "0.6");
    $("#overlayPop").css('opacity', "0.6");
    $("#overlayPop").css('-KHTML-opacity', "0.6");

    //Centra el popup
    w = (w/2) - (table_w/2);
    h = (h/2) - (table_h/2);
    $("#pop").css("left",w + "px");
    $("#pop").css("top",h + "px");

    $("#cerrar").click(function (){
        $("#pop").fadeOut('slow');
        $("#overlayPop").fadeOut('slow');
    });
    $("#pop").css('display', 'none');
});