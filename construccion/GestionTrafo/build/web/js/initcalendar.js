function GetFecha(obj){
     popcalendar.selectWeekendHoliday(1,1);
     popcalendar.show(obj, null, "2000-01-01", "Hoy");
}
function GetFechaAll(obj){
     popcalendar.selectWeekendHoliday(1,1);
     popcalendar.show(obj, null, "2000-01-01", null);
}
popcalendar = getCalendarInstance();
popcalendar.startAt = 1;
popcalendar.showWeekNumber = 0;
popcalendar.showToday = 1;
popcalendar.showWeekend = 1;
popcalendar.showHolidays = 1;
popcalendar.selectWeekend = 1;
popcalendar.selectHoliday = 1;
popcalendar.addCarnival = 0;
popcalendar.addGoodFriday = 0;
popcalendar.language = 0;
popcalendar.defaultFormat = "yyyy-mm-dd";
popcalendar.fixedX = -1;
popcalendar.fixedY = -1;
popcalendar.fade = .5;
popcalendar.shadow = 1;
popcalendar.move = 0;
popcalendar.saveMovePos = 1;
popcalendar.centuryLimit = 40;
popcalendar.imgDir="img/calendar/";
popcalendar.initCalendar();
