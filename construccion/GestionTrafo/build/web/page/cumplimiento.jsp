<jsp:useBean id="vw"     scope="page"    class="view.VwRegistro" />
<jsp:useBean id="Sesion" scope="session" class="model.ModSesion"/>
<form action="" method="post" name="FrmCumplimiento">
    <input name="frm" type="hidden" value="cumplimiento" />
    <jsp:include page="/jsp/lnk_graficos.jsp">
        <jsp:param name="info" value=""/>
    </jsp:include>
    <table cellpadding="0" cellspacing="0" width="1240">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td><input id="fechaIni" name="fechaIni" type="text" size="15" readonly="yes" class="txtFecha" value="" /></td>
                        <td><img alt=""  src="img/E_examinar.gif" width='17' height='17' style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fechaIni'));" <%=vw.verOverLib("haga click aqui, para buscar por fecha de expeditación")%> /></td>   
                        <td><input id="fechaFin" name="fechaFin" type="text" size="15" readonly="yes" class="txtFecha" value="" /></td>
                        <td><img alt=""  src="img/E_examinar.gif" width='17' height='17' style="CURSOR: pointer" onclick="GetFechaAll(getElemento('fechaFin'));" <%=vw.verOverLib("haga click aqui, para buscar por fecha de expeditación")%> /></td>   
                    </tr>
                    <tr>
                        <td><input type="submit" name="enviar" value="Enviar"></td>
                    </tr>
                </table>                       
            </td>
        </tr>
        <%=vw.cargarCumplimientosVoltajes(request)%>
        <tr>
            <td>
                <div class="jqPlot" id="chart1" style="height:350px; width:1000px;" align="center"></div>
            </td>
        </tr>
    </table>
</form>