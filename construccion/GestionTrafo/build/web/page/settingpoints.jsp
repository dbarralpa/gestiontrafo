<jsp:useBean id="vw"     scope="page"    class="view.VwRegistro" />
<jsp:useBean id="Sesion" scope="session" class="model.ModSesion"/>
<form action="" method="post" name="FrmSetearDatos">
    <input name="frm" type="hidden" value="setearDatos" />
    <table cellpadding="0" cellspacing="0" width="1240">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="top">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="500" height="203" valign="top">
                                        <table width="500" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td colspan="3">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="30" style="font-size: 18px;">SETTING POINTS</td>
                                            </tr>
                                            <tr>
                                                <td width="100" class="color4">Equipo</td>
                                                <td width="400" class="color4"><%=Sesion.getUsuario().getEmpresa().getTransformador().getDescripcion()%></td>
                                                <td width="100" class="color4">Serie:&nbsp;<%=Sesion.getUsuario().getEmpresa().getTransformador().getSerie()%></td>
                                            </tr>
                                            <tr>
                                                <td width="80" class="color5">Red</td>
                                                <td class="color5"><%=Sesion.getUsuario().getEmpresa().getTransformador().getRed()%></td>
                                                <td class="color5">Falla:&nbsp;<%=Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getNombre()%></td>
                                            </tr>
                                            <tr>
                                                <td width="80" class="color4">Par&aacute;metro</td>
                                                <td  colspan="2" class="alto color4" height="32"><%=Sesion.getUsuario().getEmpresa().getParametro().getDescripcion()%></td>
                                            </tr>
                                            <%=vw.cargarParametroSeteado(request)%>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20">&nbsp;</td>
                        <td  valign="top">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="top" width="170">
                                        <table width="180" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td valign="middle" width="170" height="20"  style="font-size: 12px">Seleccionar RED </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <table border="1" cellpadding="0" cellspacing="0"  width="170px">
                                                        <tr>
                                                            <td class="color" width="170" align="center">Denominaci&oacute;n</td>
                                                            <!--td class="color"  align="center" width="17px">&nbsp;</td-->
                                                        </tr>
                                                    </table>
                                                    <div class="ScrollStyle8">
                                                        <table cellpadding="0" cellspacing="0" border="1" width="170px">
                                                            <%=vw.cargarRedesEmpresa(request)%>                                                            
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="20">&nbsp;</td>
                        <td  valign="top">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="middle" width="520" height="20" style="font-size: 12px">Seleccionar equipo

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="ScrollStyle7">
                                            <table cellpadding="0" cellspacing="0" border="1" width="1013px">
                                                <tr>
                                                    <td class="color" rowspan="2"  align="center" width="40px">ID&nbsp;</td>
                                                    <td class="color" colspan="4"  align="center" width="490px">Identificaci&oacute;n&nbsp;</td>
                                                    <td class="color" rowspan="2"  align="center" width="120px">&Uacute;ltima Lectura</td>
                                                    <td class="color" colspan="6"  align="center"  width="315px">Indicadores de uso</td>
                                                </tr>
                                                <tr>
                                                    <td class="color"   align="center" width="250px">Descripci&oacute;n</td>
                                                    <td class="color"   align="center" width="60px">Serie</td>
                                                    <td class="color"  align="center" width="100px">F&aacute;brica</td>
                                                    <td class="color"   align="center" width="80px">Fabricaci&oacute;n</td>
                                                    <td class="color"   align="center" width="80px">Fallas</td>
                                                    <td class="color"   align="center" width="60px">Factor de Carga</td>
                                                    <td class="color"   align="center" width="80px">Factor de demanda</td>
                                                    <td class="color"   align="center" width="80px">Factor de utilizaci&oacute;n</td>
                                                    <td class="color"   align="center" width="0px">Cortes</td>
                                                    <!--td class="color"  align="center" width="15px">&nbsp;</td-->
                                                </tr>
                                            </table>
                                            <div class="ScrollStyle6">
                                                <table cellpadding="0" cellspacing="0" border="1" width="1013px">
                                                    <%=vw.grillaTransformadoresRed(request)%>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="700" colspan="2" height="30">PAR&Aacute;METROS</td>
                        <td width="40">&nbsp;</td>
                        <td width="180">TIPO</td>
                        <td width="60">&nbsp;</td>
                        <td width="280">FALLAS</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="700" colspan="2" valign="top">
                            <table cellpadding="0" cellspacing="0" width="683" border="1">
                                <tr>
                                    <td width="183" rowspan="2" valign="middle" class="color" align="center">Descripci&oacute;n</td>
                                    <td width="50" rowspan="2" valign="middle" class="color" align="center">Tipo</td>
                                    <td width="250" colspan="2" valign="middle" class="color" align="center">&Uacute;ltima modificaci&oacute;n</td>
                                    <td width="100" rowspan="2" valign="middle" class="color" align="center">Valor actual</td>
                                    <td width="100" rowspan="2" valign="middle" class="color" align="center">Origen</td>
                                    <!--td class="color" rowspan="2"  align="center" width="17px">&nbsp;</td-->
                                </tr>
                                <tr>
                                    <td width="190px" valign="middle" class="color" align="center">Fecha</td>
                                    <td width="60px" valign="middle" class="color" align="center">Usuario</td>
                                </tr>
                            </table>
                            <div class="ScrollStyle9">
                                <table cellpadding="0" cellspacing="0" border="1" width="683px">
                                    <%=vw.cargarParametros(request)%>
                                </table>
                            </div>
                        </td>
                        <td width="20">&nbsp;</td>
                        <td valign="top" width="240">
                            <table border="1" cellpadding="0" cellspacing="0" width="170">
                                <tr>
                                    <td class="color" width="170" align="center">Descripci&oacute;n</td>
                                    <!--td class="color" width="17" align="center">&nbsp;</td-->
                                </tr>
                            </table>
                            <div class="ScrollStyle10">
                                <%if(Sesion.getUsuario().getEmpresa().getTransformador().getIdTrans() > 0){%>
                                <table border="1" cellpadding="0" cellspacing="0" width="170">
                                    <tr>
                                        <td align="center">
                                            <a href="Main.do?frm=setearDatos&tipo=Z">
                                                Todo
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <a href="Main.do?frm=setearDatos&tipo=F">
                                                Fallas
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <a href="Main.do?frm=setearDatos&tipo=S">
                                                Sistema
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                                <%}else{%>
                                <table border="1" cellpadding="0" cellspacing="0" width="170">
                                    <tr>
                                        <td align="center">
                                                Todo
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                                Fallas
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                                Sistema
                                        </td>
                                    </tr>
                                </table>
                                <%}%>
                            </div>
                        </td>
                        <td valign="top" width="280">
                            <table border="1" cellpadding="0" cellspacing="0" width="263">
                                <tr>
                                    <td class="color" width="263" align="center">Descrpci&oacute;n</td>
                                    <!--td class="color" width="17" align="center">&nbsp;</td-->
                                </tr>
                            </table>
                            <div class="ScrollStyle11">
                                <table border="1" cellpadding="0" cellspacing="0" width="263">
                                    <%=vw.cargarAllFallas(request)%>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>