<%@page import="model.apl.ModVariable"%>
<jsp:useBean id="LOAD_ONSUBMIT" scope="request" class="java.lang.String" />
<jsp:useBean id="vw"     scope="page"    class="view.VwRegistro" />
<jsp:useBean id="Sesion" scope="session" class="model.ModSesion"/>
<form action="" method="post" name="FrmRegistros" <%=LOAD_ONSUBMIT%>>
    <input name="frm" type="hidden" value="registros" />
    <jsp:include page="/jsp/lnk_graficos.jsp">
        <jsp:param name="info" value=""/>
    </jsp:include>
    <jsp:include page="/jsp/lnk_popup_busqueda.jsp">
        <jsp:param name="frm" value=""/>
    </jsp:include>
    <table>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <table border="0" width="1000px">
                                <tr>
                                    <td align="center" width="1000px">
                                        <font size="2" color="#0000FF"> RELACION DE LOS FACTORES DE POTENCIA POR FASE</font>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" width="1000px">
                                        <%=vw.datosTrans(request)%>
                                    </td>
                                </tr>
                                <tr>


                                    <%if (!Sesion.getRegSelected().isEmpty()) {
                                                    int lar = 1000 - (Sesion.getRegSelected().size() * 25);%>
                                    <td width="<%=lar%>px">&nbsp;</td>
                                    <%for (int i = 0; i < Sesion.getRegSelected().size(); i++) {%>
                                    <td width="30px"  class="estil1"><%=((ModVariable) Sesion.getRegSelected().get(i)).getAbrevi()%></td>
                                    <%}%>
                                    <%} else {%>
                                    <td width="950px">&nbsp;</td>
                                    <td width="25px"  class="estil1">N/A</td>
                                    <%}%>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <div class="jqPlot" id="chart1" style="height:350px; width:1000px;" align="center"></div>
                        </td>
                    </tr>
                    <tr>
                        <td width="1000px">
                            <table>
                                <tr>
                                    <td valign="middle"><button onClick="plot.resetZoom()" class="boton">Contraer gr�fico</button></td>
                                    <td valign="middle">
                                        <a href="Main.do?mnu=estaGene">
                                            <img alt="" name="estagene" id="btn_buscar" class="boton" src="img/images/estadisticas.JPG" border="0"/>
                                        </a>
                                    </td>
                                    <td>
                                        <img alt="" border="0" name="btn_buscar" onclick="mostrarPopUp();" id="btn_buscar" src="img/images/estadofalla.jpg"/>
                                    </td>
                                </tr>                                
                            </table>
                        </td>

                    </tr>
                </table>
            </td>
            <td align="left" valign="top">
                <table align="left">
                    <tr>
                        <td height="34">&nbsp;</td>
                    </tr>
                    <%if (!Sesion.getRegSelected().isEmpty()) {%>
                    <%for (int i = 0; i < Sesion.getRegSelected().size(); i++) {%>
                    <tr>
                        <td class="estil1" valign="top"><%=((ModVariable) Sesion.getRegSelected().get(i)).getDescripcion()%></td>
                    </tr>
                    <%}
                        if (Sesion.getRegSelected().size() < 4) {
                            for (int i = 0; i < (4 - Sesion.getRegSelected().size()); i++) {%>
                    <tr>
                        <td class="estil1">&nbsp;</td>
                    </tr>
                    <%}
                        }
                    } else {%>
                    <%for (int i = 0; i < 5; i++) {%>
                    <tr>
                        <td class="estil1">&nbsp;</td>
                    </tr>
                    <%}
                                }%>
                    <tr>
                        <td>Mediciones disponibles</td>
                    </tr>
                    <tr>
                        <td class="ScrollStyle" width="219" valign="top" height="100px">
                            <DIV class="ScrollStyle5">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Contenido">
                                    <tr>
                                        <td width="100%"></td>
                                    </tr>
                                    <%=vw.registrosDisponibles(request)%>
                                </table>
                            </DIV>
                        </td>
                    </tr>
                    <tr>
                        <td height="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" name="recarga" id="recarga" value="Generar nuevo gr�fico" class="boton">
                            <a href="Main.do?mnu=home">
                                <img alt="" name="volver" id="volver" class="boton" src="img/images/volver.jpg" border="0"/>
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="480">
                                <tr>
                                    <td width="38">&nbsp;</td>
                                    <td  height="20">
                                        Valores b&aacute;sicos del per&iacute;odo
                                    </td>

                                </tr>
                            </table>
                        </td>
                        <td width="30">&nbsp;</td>
                        <td height="20">
                            Descripci�n falla
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <jsp:include page="/jsp/lnk_tablaRegistro.jsp">
                                <jsp:param name="info" value=""/>
                            </jsp:include>
                        </td>
                        <td width="30px">&nbsp;</td>
                        <td class="borde" height="108px" valign="top" width="900px">
                            <%=vw.descripcionFalla(request)%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!--/div-->
</form>