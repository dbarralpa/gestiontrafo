<jsp:useBean id="LOAD_ONSUBMIT" scope="request" class="java.lang.String" />
<jsp:useBean id="Sesion" 	scope="session" class="model.ModSesion"/>
<jsp:useBean id="vw"            scope="page"    class="view.VwRegistro" />

<link type="text/css" href="css/popup.css" rel="stylesheet" />

<script type="text/javascript" src="js/popup.js"></script>

<script type="text/javascript">
    function mostrarCriterio(obj) {
        //alert($("#obs").attr("value"));
        if(obj=='c_vista'){            
            changeValue('rbtnFiltro', 'c_vista');
            //alert(getElemento("obs").value);
            //alert($("textarea").html());
            /*$.get("Main.do?frm=jquery&c_vista=SI", function(data) {
                $("#c_criterio").find("table").html(data);
            });*/
        }else if(obj=='c_solucionada'){
            changeValue('rbtnFiltro', 'c_solucionada');
            /*$.get("Main.do?frm=jquery&c_solucionada=SI", function(data) {
                $("#c_criterio").find("table").html(data);
            });*/
        }
    }
    function cambiarEstado() {       
        var cargando = "<tr><td width=\"24\">&nbsp;</td>";
        cargando += "<td class=\"Cargando\" align=\"center\">";
        cargando += "<img alt=\"\" src=\"img/cargando.gif\" style=\"vertical-align:middle;\"></td>";
        cargando += "<td width=\"25\">&nbsp;</td></tr>";
        //$("#c_criterio table").html(cargando);
        var param = "";
        if($("#rbtnFiltro").attr("value")=='c_vista'){
            param = "&rbtnFiltro="+$("#rbtnFiltro").attr("value")+"&obs="+$("#obs").attr("value");
        }else if($("#rbtnFiltro").attr("value")=='c_solucionada'){
            param = "&rbtnFiltro="+$("#rbtnFiltro").attr("value")+"&obs="+$("#obs").attr("value");
        }
        //alert("Parametros: " + param + " Obs: " + $("#obs").attr("value"));
        $.get("Main.do?frm=jquery"+param, function(data) {
            $("#c_criterio table").html(data);
        });
    }
</script>


<div id="overlayPop" style="display:none;"></div>
<div id="pop" style="display:none;">
    <table width="350px" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3" height="50">
                <table width="350px" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="40" width="100%" colspan="2">
                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                <td align="center" width="55%"><b>Estado de la falla</b></td>
                                <!--td><img alt="" id="cerrar" <%--=vw.verOverTip("Haga click para cerrar esta ventana")--%> src="img/btn17x17/btn_cerrar.gif" border="0"></td-->
                            </table>
                        </td>
                    </tr>
                    <tr><td colspan="2"><hr width="100%"></td></tr>
                    <tr>
                        <td align="center" valign="top">
                            <fieldset>
                                <legend>Cambiar estado de la falla:</legend>
                                <table width="60%" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <%if(Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getColor().equals("fallanover")){%>
                                    <input type="radio" name="rbtnFiltro" id="rbtnFiltro" checked value="c_vista" onclick="mostrarCriterio('c_vista')" />Vista
                                    <input type="radio" name="rbtnFiltro" id="rbtnFiltro" value="c_solucionada" onclick="mostrarCriterio('c_solucionada')" />Solucionada
                                    <%}else{%>
                                    <input type="radio" name="rbtnFiltro" id="rbtnFiltro" checked value="c_solucionada" onclick="mostrarCriterio('c_solucionada')" />Solucionada
                                    <input type="radio" disabled name="rbtnFiltro" id="rbtnFiltro" value="c_vista" onclick="mostrarCriterio('c_vista')" />Vista
                                    <%}%>
                                    
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <div id="c_criterio">
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                Observación:
                            </td>
                        </tr>
                        <tr>
                            <td width="90%" align="center"  colspan="2">
                                <textarea name="obs" id="obs" cols="42" rows="2"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <img alt="" name="cerrar" id="cerrar" src="img/btn40x40/salir.JPG" border="0" />
                            </td>
                            <td align="center">
                                <img alt="" name="actualizar" id="actualizar" src="img/images/actualizar.JPG" border="0" onclick="cambiarEstado();"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <!--td colspan="2" align="center">
                <img alt="" name="btn_busqueda" id="btn_busqueda" src="img/btn40x40/btn_busqueda.gif" />
            </td-->
        </tr>
        <!--tr><td colspan="3"><hr width="100%"></td></tr-->
        <!--tr>
            <td colspan="3">
                <div id="c_resul">
                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
        <!--//=vw.devolverString()%-->
    </table>
</div>