<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<jsp:useBean id="LOAD_ONSUBMIT" scope="request" class="java.lang.String" />
<jsp:useBean id="Sesion"        scope="session" class="model.ModSesion"/>
<jsp:useBean id="vw"            scope="page" class="view.VwRegistro" />

<form method="post" action="" <%=LOAD_ONSUBMIT%>>
    <input name="frm" type="hidden" value="login" />
    <table width="160" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="15" height="5"><img alt=""  src="img/frame/fm1x1.gif" /></td>
            <td class="img_fm1x2"/>
            <td width="5" height="5"><img alt=""  src="img/frame/fm1x3.gif" /></td>
        </tr>
        <tr>
            <td><img alt=""  src="img/frame/fm2x1_3.gif" /></td>
            <td class="img_fm2x2"><strong></strong></td>
            <td><img alt=""  src="img/frame/fm2x3.gif" /></td>
        </tr>
        <tr>
            <td  class="img_fm3x1">&nbsp;</td>
            <td  class="img_fm3x2"><br />
                <%if (!Sesion.isEnSesion()) {%>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td><input name="txtUsr" type="text" id="txtUsr" class="txtLogin" tabindex="1" <%=vw.verOverTip("Digite user")%> /></td>
                        <td width="60" rowspan="2" align="right"><input name="Submit" type="image" src="img/login.gif" tabindex="3" <%=vw.verOverTip("Click para validar datos")%>/></td>
                    </tr>
                    <tr><td><input name="txtPwd" type="password" id="txtPwd" class="txtLogin" tabindex="2" <%=vw.verOverTip("Digite clave")%> /></td>
                    </tr>
                </table>
                <%} else {%>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center"><jsp:getProperty name="Sesion" property="nombreFull" /></td>
                    </tr>
                    <tr>
                        <td align="center"><input type="submit" name="LogOut.x" value="logout" /></td>
                    </tr>
                </table>
                <%}%>
                <br />
            </td>
            <td  class="img_fm3x3">&nbsp;</td>
        </tr>
        <tr>
            <td><img alt=""  src="img/frame/fm4x1.gif" /></td>
            <td class="img_fm4x2"></td>
            <td><img alt=""  src="img/frame/fm4x3.gif" /></td>
        </tr>
    </table>

</form>    