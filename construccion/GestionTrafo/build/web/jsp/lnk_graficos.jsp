<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<jsp:useBean id="Sesion" scope="session" class="model.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="view.VwRegistro" />


<link rel="stylesheet" type="text/css" href="css/jquery.jqplot.css" />
<script language="javascript" type="text/javascript" src="js/jqplot/jquery-1.4.4.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/jquery-1.4.4.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/jquery.jqplot.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
<!--script language="javascript" type="text/javascript" src="js/jqplot/plugins/jqplot.canvasOverlay.js"></script-->
<script language="javascript" type="text/javascript" src="js/jqplot/jqplot.enhancedLegendRenderer.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/jqplot.enhancedLegendRenderer.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/jqplot.cursor.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/jqplot.dateAxisRenderer.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/excanvas.js"></script>
<script language="javascript" type="text/javascript" src="js/jqplot/excanvas.min.js"></script>

<%=vw.scriptGrafico(request)%>