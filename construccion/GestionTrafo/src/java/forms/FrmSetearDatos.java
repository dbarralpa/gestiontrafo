package forms;

import action.MainReq;
import controllers.PrmApp;
import controllers.PrmDato;
import controllers.PrmGrabar;
import java.sql.Timestamp;
import java.util.ArrayList;
import javax.servlet.http.*;
import model.ModSesion;
import model.apl.ModFalla;
import model.apl.ModParametro;
import model.apl.ModRed;
import model.apl.ModTransformador;
import org.apache.struts.action.*;

public class FrmSetearDatos extends MainReq {

    @Override
    public ActionForward execute(ActionMapping amap, ActionForm afrm, HttpServletRequest req, HttpServletResponse res) {
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            if (req.getParameter("tipo") != null) {
                Sesion.getUsuario().getEmpresa().getTransformador().setFalla(new ModFalla());
                Sesion.getUsuario().getEmpresa().setParametro(new ModParametro());
                Sesion.setAnalisisTiempo(new ArrayList<ModParametro>());
                Sesion.getUsuario().getEmpresa().setParametros(new ArrayList<ModParametro>());
                ArrayList<ModParametro> params = PrmDato.cargarParametros(0,
                        Sesion.getUsuario().getEmpresa().getTransformador().getIdTrans(),
                        Sesion.getUsuario().getEmpresa().getIdEmpre(),
                        req.getParameter("tipo"));
                Sesion.getUsuario().getEmpresa().setParametros(params);

            }
            if (req.getParameter("param") != null) {
                Sesion.getUsuario().getEmpresa().setAllFallas(PrmDato.cargarAllFallas());
                if (Sesion.getUsuario().getEmpresa().getTransformador().getIdTrans() > 0) {
                    ArrayList<ModParametro> params = PrmDato.cargarParametros(Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getIdFalla(),
                            Sesion.getUsuario().getEmpresa().getTransformador().getIdTrans(),
                            Sesion.getUsuario().getEmpresa().getIdEmpre(), null);
                    Sesion.getUsuario().getEmpresa().setParametros(params);
                }
            }
            if (req.getParameter("idFalla") != null) {
                Sesion.getUsuario().getEmpresa().getTransformador().setFalla(new ModFalla());
                Sesion.getUsuario().getEmpresa().setParametro(new ModParametro());
                Sesion.setAnalisisTiempo(new ArrayList<ModParametro>());
                Sesion.getUsuario().getEmpresa().getParametros().clear();
                int idFalla = Integer.parseInt(req.getParameter("idFalla"));
                ModFalla falla = (ModFalla) Sesion.getUsuario().getEmpresa().getAllFallas().get(idFalla);
                Sesion.getUsuario().getEmpresa().getTransformador().setFalla(falla);
                ArrayList<ModParametro> params = PrmDato.cargarParametros(Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getIdFalla(),
                        Sesion.getUsuario().getEmpresa().getTransformador().getIdTrans(),
                        Sesion.getUsuario().getEmpresa().getIdEmpre(), null);
                Sesion.getUsuario().getEmpresa().setParametros(params);
            }
            if (req.getParameter("idTransformador") != null) {
                Sesion.getUsuario().getEmpresa().getTransformador().setFalla(new ModFalla());
                Sesion.getUsuario().getEmpresa().setParametro(new ModParametro());
                Sesion.setAnalisisTiempo(new ArrayList<ModParametro>());
                Sesion.getUsuario().getEmpresa().setParametros(new ArrayList<ModParametro>());
                int idTransfo = Integer.parseInt(req.getParameter("idTransformador"));
                ModTransformador trans = (ModTransformador) Sesion.getUsuario().getEmpresa().getTransformadores().get(idTransfo);
                Sesion.getUsuario().getEmpresa().setTransformador(trans);
                ArrayList<ModParametro> params = PrmDato.cargarParametros(Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getIdFalla(),
                        Sesion.getUsuario().getEmpresa().getTransformador().getIdTrans(),
                        Sesion.getUsuario().getEmpresa().getIdEmpre(),
                        null);
                Sesion.getUsuario().getEmpresa().setParametros(params);
            }
            if (req.getParameter("idParametros") != null) {
                Sesion.setAnalisisTiempo(new ArrayList<ModParametro>());
                int idParametros = Integer.parseInt(req.getParameter("idParametros"));
                ModParametro param = Sesion.getUsuario().getEmpresa().getParametros().get(idParametros);
                Sesion.getUsuario().getEmpresa().setParametro(param);
                boolean dato = PrmDato.cargarLimitesParametros(Sesion.getUsuario().getEmpresa().getTransformador().getIdTrans(), Sesion.getUsuario().getEmpresa().getIdEmpre(), Sesion.getUsuario().getEmpresa().getParametro());
                if (dato && Sesion.getUsuario().getEmpresa().getParametro().getIdParam() == 44) {
                    Sesion.setAnalisisTiempo(PrmDato.cargarAnalisisTiempo());
                }

            }
            if (req.getParameter("idRed") != null) {
                Sesion.getUsuario().getEmpresa().getTransformador().setFalla(new ModFalla());
                Sesion.getUsuario().getEmpresa().setParametro(new ModParametro());
                Sesion.setAnalisisTiempo(new ArrayList<ModParametro>());
                Sesion.getUsuario().getEmpresa().setParametros(new ArrayList<ModParametro>());
                int idRed = Integer.parseInt(req.getParameter("idRed"));
                Sesion.getUsuario().getEmpresa().getTransformadoresRed().clear();
                ModRed rede = Sesion.getUsuario().getEmpresa().getRedes().get(idRed);
                Sesion.getUsuario().getEmpresa().setRed(rede);
                ArrayList<ModTransformador> trans = PrmDato.cargarTransformadoresRed(rede.getIdRed());
                for (int i = 0; i < trans.size(); i++) {
                    for (int a = 0; a < Sesion.getUsuario().getEmpresa().getTransformadores().size(); a++) {
                        ModTransformador tranfo = (ModTransformador) Sesion.getUsuario().getEmpresa().getTransformadores().get(a);
                        if (trans.get(i).getIdTrans() == tranfo.getIdTrans()) {
                            Sesion.getUsuario().getEmpresa().getTransformadoresRed().add(tranfo);
                        }
                    }
                }
            }
            if (req.getParameter("estandarequipo") != null) {
                if (!req.getParameter("estandarequipov").equals("")) {
                    Timestamp fec = new Timestamp(new java.util.Date().getTime());
                    double valor = Double.parseDouble(req.getParameter("estandarequipov"));
                    if (valor == Sesion.getUsuario().getEmpresa().getParametro().getMax() && Sesion.getUsuario().getEmpresa().getParametro().getArea().equals("equipo")) {
                        Sesion.setTip("El valor del parámetro no debe ser el mismo ", 1);
                    } else {
                        boolean est = PrmGrabar.actualizarParametros("equipo", valor, Sesion.getUsuario(), fec);
                        if (est) {
                            Sesion.getUsuario().getEmpresa().getParametro().setArea("equipo");
                            Sesion.getUsuario().getEmpresa().getParametro().setMax(valor);
                            Sesion.getUsuario().getEmpresa().getParametro().setMaxTrans(valor);
                            Sesion.getUsuario().getEmpresa().getParametro().setFecha(fec);
                            Sesion.setTip("Parámetro actualizado", 0);
                        } else {
                            Sesion.setTip("Parámetro no actualizado", 1);
                        }

                    }
                } else {
                    Sesion.setTip("Debe agregar un valor al parámetro", 1);
                }

            }
            if (req.getParameter("estandarempresa") != null) {
                if (!req.getParameter("estandarempresav").equals("")) {
                    Timestamp fec = new Timestamp(new java.util.Date().getTime());
                    double valor = Double.parseDouble(req.getParameter("estandarempresav"));
                    boolean est = PrmGrabar.actualizarParametros("empresa", valor, Sesion.getUsuario(), fec);
                    if (valor == Sesion.getUsuario().getEmpresa().getParametro().getMax() && Sesion.getUsuario().getEmpresa().getParametro().getArea().equals("empresa")) {
                        Sesion.setTip("El valor del parámetro no debe ser el mismo ", 1);
                    } else {
                        if (est) {
                            Sesion.getUsuario().getEmpresa().getParametro().setArea("empresa");
                            Sesion.getUsuario().getEmpresa().getParametro().setMax(valor);
                            Sesion.getUsuario().getEmpresa().getParametro().setMaxEmpre(valor);
                            Sesion.getUsuario().getEmpresa().getParametro().setFecha(fec);
                            Sesion.setTip("Parametro actualizado", 0);
                        } else {
                            Sesion.setTip("Parámetro no actualizado", 1);
                        }
                    }
                } else {
                    Sesion.setTip("Debe agregar un valor al parámetro", 1);
                }
            }
            if (req.getParameter("estandartrafonet") != null) {
                if (!req.getParameter("estandartrafonetv").equals("")) {
                    Timestamp fec = new Timestamp(new java.util.Date().getTime());
                    double valor = Double.parseDouble(req.getParameter("estandartrafonetv"));
                    boolean est = PrmGrabar.actualizarParametros("trafonet", valor, Sesion.getUsuario(), fec);
                     if (Sesion.getUsuario().getEmpresa().getParametro().getArea().equals("trafonet")) {
                        Sesion.setTip("El valor del parámetro no debe ser el mismo ", 1);
                    } else {
                    if (est) {
                        Sesion.getUsuario().getEmpresa().getParametro().setArea("trafonet");
                        Sesion.getUsuario().getEmpresa().getParametro().setMax(valor);
                        Sesion.getUsuario().getEmpresa().getParametro().setMaxTrafonet(valor);
                        Sesion.getUsuario().getEmpresa().getParametro().setFecha(fec);
                        Sesion.setTip("Parametro actualizado", 0);
                    } else {
                        Sesion.setTip("Parámetro no actualizado", 1);
                    }
                    }
                } else {
                    Sesion.setTip("Debe agregar un valor al parámetro", 1);
                }
            }
        } catch (Exception ex) {
            Sesion.setTip("Error General al Procesar Informacion.<br> contactese con su administrador de servicios.", 1);
            PrmApp.addError(ex, true);
        }

        req.getSession().setAttribute("Sesion", Sesion);


        super.SendtoHome(res, req);
        return null;
    }
}
