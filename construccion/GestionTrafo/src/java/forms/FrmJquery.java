package forms;

import action.MainReq;
import controllers.PrmApp;
import controllers.PrmGrabar;
import java.io.*;
import java.sql.Timestamp;
import java.util.Date;
import javax.servlet.http.*;
import model.ModSesion;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import view.VwRegistro;

public class FrmJquery extends MainReq {

    public ActionForward execute(ActionMapping amap, ActionForm afrm, HttpServletRequest req, HttpServletResponse res) {
        try {
            res.setContentType("text/html;charset=UTF-8");
            res.addHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            res.addHeader("pragma", "no-cache");
            res.addDateHeader("Expires", -1);
            req.setCharacterEncoding("UTF-8");
            res.setCharacterEncoding("UTF-8");

            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            VwRegistro vw = new VwRegistro();
            PrintWriter out = res.getWriter();
            String valor = "";
            /*if (req.getParameter("c_solucionada") != null) {

                out.println(vw.fallaSolucionada());
            }*/ if (req.getParameter("rbtnFiltro") != null && req.getParameter("rbtnFiltro").equals("c_vista")) {
                if(req.getParameter("obs") != null && !req.getParameter("obs").equals("")){
                boolean esta = PrmGrabar.actualizarEstadoFalla((byte) 2,(byte) Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getIdFallaTrans(),
                                                                   new Timestamp(new Date().getTime()),
                                                                   req.getParameter("obs"),
                                                                   Sesion.getUsuario().getUser(),Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getLectura());
                if(esta){
                valor = "SE ACTUALIZÓ CORRECTAMENTE";
                }else{
                valor = "NO SE PUDO ACTUALIZAR EL ESTADO";
                }
                
                out.println(vw.estadoFallaSolucionada(esta,(byte) 2,valor));
                }else{
                    valor = "DEBE COLOCAR UNA OBSERVACIÓN";
                    out.println(vw.estadoFallaSolucionada(false,(byte) 2,valor));
                }
                
            } else if (req.getParameter("rbtnFiltro") != null && req.getParameter("rbtnFiltro").equals("c_solucionada")) {
                if(req.getParameter("obs") != null && !req.getParameter("obs").equals("")){
                boolean esta = PrmGrabar.actualizarEstadoFalla((byte) 3,(byte) Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getIdFallaTrans(),
                                                                   new Timestamp(new Date().getTime()),
                                                                   req.getParameter("obs"),
                                                                   Sesion.getUsuario().getUser(),Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getLectura());
                if(esta){
                valor = "SE ACTUALIZÓ CORRECTAMENTE";
                }else{
                valor = "NO SE PUDO ACTUALIZAR EL ESTADO";
                }

                out.println(vw.estadoFallaSolucionada(esta,(byte) 3,valor));
                }else{
                    valor = "DEBE COLOCAR UNA OBSERVACIÓN";
                    out.println(vw.estadoFallaSolucionada(false,(byte) 3,valor));
                }
            }
            out.close();
            req.getSession().setAttribute("Sesion", Sesion);
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        req.getSession().setAttribute("Sesion", Sesion);
        return null;
    }
}
