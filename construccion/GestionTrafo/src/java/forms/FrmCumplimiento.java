package forms;

import action.MainReq;
import controllers.PrmApp;
import controllers.PrmDato;
import controllers.PrmRegistro;
import java.util.ArrayList;
import javax.servlet.http.*;
import model.ModSesion;
import model.apl.ModTransformador;
import model.apl.ModVariable;
import org.apache.struts.action.*;

public class FrmCumplimiento extends MainReq {

    @Override
    public ActionForward execute(ActionMapping amap, ActionForm afrm, HttpServletRequest req, HttpServletResponse res) {
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            if (req.getParameter("state") != null) {
                Sesion.getRegSelected().clear();
                Sesion.setCargarParteGrafico("");
                Sesion.setMin(1.1);
                Sesion.setMax(1.1);
                Sesion.getUsuario().getEmpresa().getTransformador().setDetalles(new ArrayList());
            }

            if (req.getParameter("enviar") != null) {
                Sesion.getRegSelected().clear();
                Sesion.setCargarParteGrafico("");
                Sesion.setMin(1.1);
                Sesion.setMax(1.1);
                for (int i = 0; i < Sesion.getVariables().size(); i++) {
                    ModVariable varia = (ModVariable) Sesion.getVariables().get(i);
                    varia.setEstado(false);
                }
                
                ModTransformador trans = (ModTransformador) Sesion.getUsuario().getEmpresa().getTransformadores().get(0);
                Sesion.getUsuario().getEmpresa().setTransformador(trans);
                Sesion.getUsuario().getEmpresa().getTransformador().setDetalles(new ArrayList());
                trans.setDetalles(PrmDato.cargarDatosVoltaje(req.getParameter("fechaIni"), req.getParameter("fechaFin"), trans.getNomTabla(), Sesion.getUsuario().getEmpresa().getSchema(), Sesion));
                ModVariable vari = (ModVariable) Sesion.getVariables().get(0);
                vari.setEstado(true);
                ModVariable vari1 = (ModVariable) Sesion.getVariables().get(1);
                vari1.setEstado(true);
                ModVariable vari2 = (ModVariable) Sesion.getVariables().get(2);
                vari2.setEstado(true);
                Sesion.setRegSelected(vari);
                Sesion.setRegSelected(vari1);
                Sesion.setRegSelected(vari2);
                Sesion.setCargarParteGrafico(PrmRegistro.devolverGrafico(req, trans));
                PrmRegistro.cumplimientoVoltaje(trans);

            }

        } catch (Exception ex) {
            Sesion.setTip("Error General al Procesar Informacion.<br> contactese con su administrador de servicios.", 1);
            PrmApp.addError(ex, true);
        }

        req.getSession().setAttribute("Sesion", Sesion);


        super.SendtoHome(res, req);
        return null;
    }
}
