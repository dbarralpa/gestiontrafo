package forms;

import action.MainReq;
import controllers.PrmApp;
import controllers.PrmDato;
import controllers.PrmRegistro;
import javax.servlet.http.*;
import model.ModSesion;
import model.apl.ModFalla;
import model.apl.ModTransformador;
import model.apl.ModVariable;
import org.apache.struts.action.*;

public class FrmRegistros extends MainReq {

    @Override
    public ActionForward execute(ActionMapping amap, ActionForm afrm, HttpServletRequest req, HttpServletResponse res) {
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
             if (req.getParameter("transfo") != null) {
                for(int i=0;i<Sesion.getVariables().size();i++){
                    ModVariable varia = (ModVariable)Sesion.getVariables().get(i);
                    varia.setEstado(false);
                }
                Sesion.getRegSelected().clear();
                Sesion.setCargarParteGrafico("");
                Sesion.setMin(1.1);
                Sesion.setMax(1.1);
                int idTransfo = Integer.parseInt(req.getParameter("transfo"));
                int idFalla = Integer.parseInt(req.getParameter("falla"));
                ModTransformador trans = (ModTransformador) Sesion.getUsuario().getEmpresa().getTransformadores().get(idTransfo);
                ModFalla falla = (ModFalla) trans.getFallas().get(idFalla);
                trans.setDetalles(PrmDato.cargarDatosGrafico(Sesion.getUsuario().getEmpresa().getSchema(), trans.getNomTabla() + "s",
                                                             falla.getLectura(),falla.getDias()));
                Sesion.getUsuario().getEmpresa().setTransformador(trans);
                trans.setFalla(falla);
                if (trans.getFalla().getIdFalla() == 1) {
                    Sesion.getRegSelected().clear();
                    ModVariable vari = (ModVariable) Sesion.getVariables().get(0);
                    vari.setEstado(true);
                    ModVariable vari1 = (ModVariable) Sesion.getVariables().get(1);
                    vari1.setEstado(true);
                    ModVariable vari2 = (ModVariable) Sesion.getVariables().get(2);
                    vari2.setEstado(true);
                    Sesion.setRegSelected(vari);
                    Sesion.setRegSelected(vari1);
                    Sesion.setRegSelected(vari2);
                    Sesion.setCargarParteGrafico(PrmRegistro.devolverGrafico(req, trans));
                }

            }else if (req.getParameter("recarga") != null) {
                byte a = 0;
                for (int i = 0; i < Sesion.getVariables().size(); i++) {
                    if (req.getParameter("variable" + i) != null) {                        
                        a++;
                    }
                }
                if(a<=4){
                Sesion.getRegSelected().clear();
                for (int i = 0; i < Sesion.getVariables().size(); i++) {
                    ModVariable variable = ((ModVariable)Sesion.getVariables().get(i));
                    if (req.getParameter("variable" + i) != null) {
                        variable.setEstado(true);
                        Sesion.setRegSelected(variable);
                    }else{
                        variable.setEstado(false);
                    }
                }
                Sesion.setCargarParteGrafico(PrmRegistro.devolverGrafico(req, Sesion.getUsuario().getEmpresa().getTransformador()));
              }else{
                   Sesion.setTip("La cantidad de variables seleccionadas no debe ser mayor a 4", 0);
              }
            }else if (req.getParameter("detalleFalla") != null) {

             Sesion.getUsuario().getEmpresa().getTransformador().getFalla().setBitacora(PrmDato.cargarBitacoraFallas(Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getIdFalla()));
            }


        } catch (Exception ex) {
            Sesion.setTip("Error General al Procesar Informacion.<br> contactese con su administrador de servicios.", 1);
            PrmApp.addError(ex, true);
        }
        req.getSession().setAttribute("Sesion", Sesion);
        super.SendtoHome(res, req);
        return null;
    }
}
