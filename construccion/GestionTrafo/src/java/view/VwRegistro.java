package view;

import controllers.PrmApp;
import controllers.PrmRegistro;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import model.ModSesion;
import model.apl.ModBitacoraFalla;
import model.apl.ModEstadistica;
import model.apl.ModFalla;
import model.apl.ModParametro;
import model.apl.ModRed;
import model.apl.ModTransformador;
import model.apl.ModVariable;

public class VwRegistro extends PrmApp {

    public String scriptGrafico(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html += " <script type=\"text/javascript\" language=\"javascript\"> " + "\n";
            html += " $(document).ready(function(){ " + "\n";
            html += " $.jqplot.config.enablePlugins = true; " + "\n";

            html += Sesion.getCargarParteGrafico();
            String series = "";
            String yasis = "}, " + "\n";
            for (int i = 0; i < Sesion.getRegSelected().size(); i++) {
                series += "{label:'" + ((ModVariable) Sesion.getRegSelected().get(i)).getAbrevi() + "',yaxis:'y" + (i + 2) + "axis',breakOnNull: true,neighborThreshold:-1,color:'"+PrmRegistro.colorRegistro()[i]+"'},";
                if (Sesion.getMin() != 1.1) {
                    yasis += "       y" + (i + 2) + "axis:{";
                    yasis += "       min:" + Sesion.getMin() + ", ";
                    yasis += "       max:" + Sesion.getMax() + ",";
                    yasis += "       numberTicks:9,";
                    yasis += "       tickOptions:{showGridline:true}";
                    //yasis += "       rendererOptions: { forceTickAt0: true, forceTickAt100: true }";
                    yasis += "       }, ";
                } else {
                    yasis = "} " + "\n";
                }
            }
            if (series.equals("")) {
                series += "{label:'VA',yaxis:'y2axis'},{label:'CA', yaxis:'y3axis'},";
            }
            /*html += " seriesColors: [ '#4bb2c5', '#c5b47f', '#EAA228', '#579575', '#839557', '#958c12',";
            html += "'#953579', '#4b5de4', '#d8b83f', '#ff5800', '#0085cc'],";*/
            html += " series: [" + series.substring(0, series.length() - 1) + "], " + "\n";
            html += "   seriesDefaults:{ " + "\n";
            html += "         showBorders: true," + "\n";
            html += "         lineWidth:1.5," + "\n";
            html += "         showMarker: true," + "\n";
            html += "         markerRenderer: $.jqplot.MarkerRenderer," + "\n";
            html += "         markerOptions: { " + "\n";
            html += "             show: false," + "\n";
            html += "             style: 'diamond'," + "\n";
            html += "             lineWidth: 3," + "\n";
            html += "             size: 5 " + "\n";
            html += "        } " + "\n";
            html += "    }, " + "\n";
            html += " axesDefaults:{useSeriesColor: true, tickOptions:{formatString:'%d',mark: 'outside',fontSize:10,fontcolor:\"black\"}}, " + "\n";
            html += " axes: { " + "\n";
            html += "        xaxis: { " + "\n";
            html += "            renderer:$.jqplot.DateAxisRenderer, " + "\n";
            html += "            autoscale: false, ";
            html += "            tickInterval: '" + Sesion.getCantRegistros() + " minutes', " + "\n";
            html += "            tickOptions:{formatString:'%Y/%#m/%#d %I:%M:%S %p',fontSize:10} " + "\n";
            html += "        }, " + "\n";
            html += "        yaxis: { " + "\n";
            //html += "            pad: 0.3,";
            html += "            renderer: $.jqplot.LogAxisRenderer, " + "\n";
            html += "            tickOptions:{show: true,formatString:'%Y',fontSize:20} " + "\n";
            
            html += yasis;
            /* html += "        }, " + "\n";
            html += "       y2axis:{";
            html += "       min:211, ";
            html += "       max:232,"; 
            html += "       numberTicks:9,"; 
            html += "       tickOptions:{showGridline:true}";
            html += "       }, ";
            html += "       y3axis:{},";
            html += "       y4axis:{}";*/
            html += "    }, " + "\n";
            
          /*  html += "    canvasOverlay: {";
            html += "      show: true,";
            html += "      objects: [";
            html += "      {horizontalLine: {";
            html += "       name: 'pebbles',";
            html += "       y: 210,";
            html += "       lineWidth: 3,";
            html += "       color: 'rgb(100, 55, 124)',";
            html += "       shadow: true,";
            html += "       lineCap: 'butt',";
            html += "       xOffset: 0";
            html += "       }},";
            html += "       {dashedHorizontalLine: {";
            html += "       name: 'bam-bam',";
            html += "       y: 230,";
            html += "       lineWidth: 4,";
            html += "       dashPattern: [8, 16],";
            html += "       lineCap: 'round',";
            html += "       xOffset: '25',";
            html += "       color: 'rgb(66, 98, 144)',";
            html += "       shadow: false";
            html += "       }}";
            html += "      ]";
            html += "      },";*/
            
            html += " cursor:{ " + "\n";
            html += "        show:true, " + "\n";
            html += "        showVerticalLine:true, " + "\n";
            html += "        showHorizontalLine:true, " + "\n";
            html += "        showCursorLegend:false, " + "\n";
            html += "        showTooltip: true, " + "\n";
            html += "        showTooltipDataPosition: true, ";
            html += "        tooltipFormatString: '%s x:%s, y:%s', " + "\n";
            html += "        zoom:true, " + "\n";
            html += "        tooltipOffset: 40, " + "\n";
            html += "        followMouse: true, " + "\n";
            html += "        tooltipLocation: 's', " + "\n";
            html += "        placement:'inside' " + "\n";
            html += "     } " + "\n";
            html += "}); " + "\n";
            html += "}); " + "\n";
            html += "</script> " + "\n";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='5' align='center' class=\"ConBorder\">";
            html += "</td>\n" + "</tr>\n";
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String cargarDatosVariable(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            ModTransformador trans = Sesion.getUsuario().getEmpresa().getTransformador();


            html += "<table border=\"0\" width=\"480\" cellpadding=\"0\" cellspacing=\"0\"> ";
            html += "<tr>";
            html += "<td valign=\"top\" style=\"padding-top:45px\">";
            html += "<table cellpadding=\"0\" cellspacing=\"0\" border=\"1\">";
            html += "<tr>";
            html += "<td align=\"center\" valign=\"middle\" class=\"color1\" height=\"31\">";
            html += "M&iacute;nimo";
            html += "</td>";
            html += "</tr>";
            html += "<tr>";
            html += "<td align=\"center\" valign=\"middle\" class=\"color1\" height=\"32\">";
            html += "M&aacute;ximo";
            html += "</td>";
            html += "</tr>";
            html += "</table>";
            html += "</td>";
            html += "<td>";
            html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
            html += "<tr>";
            html += "    <td colspan=\"3\" align=\"center\" valign=\"middle\" height=\"30\" width=\"200\" class=\"color1\">Voltajes por fase [V] </td>";
            html += "    <td colspan=\"3\" align=\"center\" valign=\"middle\" height=\"30\" width=\"200\" class=\"color1\">Corrientes por fase[A] </td>";
            html += "    <td rowspan=\"2\" align=\"center\" valign=\"middle\" width=\"80\" class=\"color3\">corriente de neutro[A] </td>";
            html += "</tr>";
            html += "  <tr>";
            html += "    <td align=\"center\" valign=\"middle\" class=\"color2\">A</td>";
            html += "    <td align=\"center\" valign=\"middle\" class=\"color2\">B</td>";
            html += "    <td align=\"center\" valign=\"middle\" class=\"color2\">C</td>";
            html += "    <td align=\"center\" valign=\"middle\" class=\"color2\">A</td>";
            html += "    <td align=\"center\" valign=\"middle\" class=\"color2\">B</td>";
            html += "    <td align=\"center\" valign=\"middle\" class=\"color2\">C</td>";
            html += "  </tr>";
            html += "  <tr>";
            if (((ModEstadistica) trans.getEstadisticas().get(0)).getMin() >= ((ModEstadistica) trans.getEstadisticas().get(0)).getMinLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(0)).getMin()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(0)).getMinLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#FFC\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(0)).getMin()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(0)).getMinLim() + ")</td>";
            }
            if (((ModEstadistica) trans.getEstadisticas().get(1)).getMin() >= ((ModEstadistica) trans.getEstadisticas().get(1)).getMinLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(1)).getMin()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(1)).getMinLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#F00\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(1)).getMin()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(1)).getMinLim() + ")</td>";
            }
            if (((ModEstadistica) trans.getEstadisticas().get(2)).getMin() >= ((ModEstadistica) trans.getEstadisticas().get(2)).getMinLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(2)).getMin()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(2)).getMinLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#F00\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(2)).getMin()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(2)).getMinLim() + ")</td>";
            }
            if (((ModEstadistica) trans.getEstadisticas().get(3)).getMin() >= ((ModEstadistica) trans.getEstadisticas().get(3)).getMinLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(3)).getMin()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(3)).getMinLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#F00\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(3)).getMin()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(3)).getMinLim() + ")</td>";
            }
            if (((ModEstadistica) trans.getEstadisticas().get(4)).getMin() >= ((ModEstadistica) trans.getEstadisticas().get(4)).getMinLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(4)).getMin()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(4)).getMinLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#F00\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(4)).getMin()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(4)).getMinLim() + ")</td>";
            }
            if (((ModEstadistica) trans.getEstadisticas().get(5)).getMin() >= ((ModEstadistica) trans.getEstadisticas().get(5)).getMinLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(5)).getMin()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(5)).getMinLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#F00\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(5)).getMin()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(5)).getMinLim() + ")</td>";
            }
            if (((ModEstadistica) trans.getEstadisticas().get(6)).getMin() >= ((ModEstadistica) trans.getEstadisticas().get(6)).getMinLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(6)).getMin()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(6)).getMinLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#F00\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(6)).getMin()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(6)).getMinLim() + ")</td>";
            }
            html += "</tr>";
            html += "<tr>";
            if (((ModEstadistica) trans.getEstadisticas().get(0)).getMax() <= ((ModEstadistica) trans.getEstadisticas().get(0)).getMaxLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(0)).getMax()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(0)).getMaxLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#F00\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(0)).getMax()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(0)).getMaxLim() + ")</td>";
            }
            if (((ModEstadistica) trans.getEstadisticas().get(1)).getMax() <= ((ModEstadistica) trans.getEstadisticas().get(1)).getMaxLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(1)).getMax()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(1)).getMaxLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#F00\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(1)).getMax()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(1)).getMaxLim() + ")</td>";
            }
            if (((ModEstadistica) trans.getEstadisticas().get(2)).getMax() <= ((ModEstadistica) trans.getEstadisticas().get(2)).getMaxLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(2)).getMax()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(2)).getMaxLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#F00\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(2)).getMax()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(2)).getMaxLim() + ")</td>";
            }
            if (((ModEstadistica) trans.getEstadisticas().get(3)).getMax() <= ((ModEstadistica) trans.getEstadisticas().get(3)).getMaxLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(3)).getMax()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(3)).getMaxLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#F00\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(3)).getMax()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(3)).getMaxLim() + ")</td>";
            }
            if (((ModEstadistica) trans.getEstadisticas().get(4)).getMax() <= ((ModEstadistica) trans.getEstadisticas().get(4)).getMaxLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(4)).getMax()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(4)).getMaxLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#F00\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(4)).getMax()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(4)).getMaxLim() + ")</td>";
            }
            if (((ModEstadistica) trans.getEstadisticas().get(5)).getMax() <= ((ModEstadistica) trans.getEstadisticas().get(5)).getMaxLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(5)).getMax()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(5)).getMaxLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#F00\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(5)).getMax()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(5)).getMaxLim() + ")</td>";
            }
            if (((ModEstadistica) trans.getEstadisticas().get(6)).getMax() <= ((ModEstadistica) trans.getEstadisticas().get(6)).getMaxLim()) {
                html += "<td class=\"estil2\" align=\"center\" valign=\"middle\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(6)).getMax()) + "</br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(6)).getMaxLim() + ")</td>";
            } else {
                html += "<td class=\"estilF2\" align=\"center\" valign=\"middle\"><font style=\"color:#F00\">" + nf.format(((ModEstadistica) trans.getEstadisticas().get(6)).getMax()) + "</font></br>";
                html += "(" + ((ModEstadistica) trans.getEstadisticas().get(6)).getMaxLim() + ")</td>";
            }
            html += "</tr>";
            html += "</table>";
            html += "</td>";
            html += "</tr>";
            html += "</table>";






        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String cargarEstadisticasTransformadoresPorFalla1(HttpServletRequest req) {
        String html = "<table  id=\"mytable\" cellpadding=\"0\" cellspacing=\"0\"> ";
        html += "<tr>";
        ModSesion Sesion = null;
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        int a = 0;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            byte l = 0;
            ModTransformador trans = Sesion.getUsuario().getEmpresa().getTransformador();
            for (int i = 0; i < trans.getFalla().getEstadisticasFalla1().size(); i++) {
                ModEstadistica estadis = trans.getFalla().getEstadisticasFalla1().get(i);
                if (estadis.getIdEsta() == 0 || estadis.getIdEsta() == 1 || estadis.getIdEsta() == 2) {
                    if (a == 0) {
                        html += "<td>";
                        html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                        html += "<tr>";
                        html += "<th class=\"nobg\">&nbsp</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Max</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Min</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Prom</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Varia</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Tiempo outBand</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">% outband</th>";
                        html += "</tr>";
                        html += "</table>";
                        html += "</td>";
                    }
                    html += "<td>";
                    html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                    html += "<tr>";
                    html += "<th class=\"spec\">" + Sesion.getVariables().get(i).getAbrevi() + "</th>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMax()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMin()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getProm()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getDesv()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getCantOutBand()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getOutBanda()) + "</td>";
                    html += "</tr>";
                    html += "</table>";
                    html += "</td>";
                    a++;
                }
            }
            html += "<td width=\"20\">&nbsp</td>";
            for (int i = 0; i < trans.getEstadisticas().size(); i++) {
                ModEstadistica estadis = trans.getEstadisticas().get(i);
                if (estadis.getIdEsta() == 100 || estadis.getIdEsta() == 101 || estadis.getIdEsta() == 102 || estadis.getIdEsta() == 103) {
                    if (a == 0) {
                        html += "<td>";
                        html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                        html += "<tr>";
                        html += "<th class=\"nobg\">&nbsp</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Max</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Min</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Prom</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Varia</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Tiempo outBand</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">% outband</th>";
                        html += "</tr>";
                        html += "</table>";
                        html += "</td>";
                    }
                    html += "<td>";
                    html += "<table  border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                    html += "<tr>";
                    html += "<th class=\"spec\">" + Sesion.getVariables().get(i).getAbrevi() + "</th>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMax()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMin()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getProm()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getDesv()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getCantOutBand()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getOutBanda()) + "</td>";
                    html += "</tr>";
                    html += "</table>";
                    html += "</td>";
                    a++;
                }
            }
            html += "</tr>";
            html += "<tr>";
            html += "<td height=\"20\">&nbsp</td>";
            html += "</tr>";
            html += "<tr>";
            a = 0;
            for (int i = 0; i < trans.getEstadisticas().size(); i++) {
                ModEstadistica estadis = trans.getEstadisticas().get(i);
                if (estadis.getIdEsta() == 600) {
                    if (a == 0) {
                        html += "<td>";
                        html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                        html += "<tr>";
                        html += "<th class=\"nobg\">&nbsp</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Max</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Min</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Prom</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Varia</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Tiempo outBand</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">% outband</th>";
                        html += "</tr>";
                        html += "</table>";
                        html += "</td>";
                    }
                    html += "<td>";
                    html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                    html += "<tr>";
                    html += "<th class=\"spec\">" + Sesion.getVariables().get(i).getAbrevi() + "</th>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMax()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMin()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getProm()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getDesv()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getCantOutBand()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getOutBanda()) + "</td>";
                    html += "</tr>";
                    html += "</table>";
                    html += "</td>";
                    a++;
                }
            }
            html += "<td width=\"20\">&nbsp</td>";
            for (int i = 0; i < trans.getEstadisticas().size(); i++) {
                ModEstadistica estadis = trans.getEstadisticas().get(i);
                if (estadis.getIdEsta() == 500 || estadis.getIdEsta() == 501 || estadis.getIdEsta() == 502) {
                    if (a == 0) {
                        html += "<td>";
                        html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                        html += "<tr>";
                        html += "<th class=\"nobg\">&nbsp</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Max</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Min</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Prom</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Varia</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Tiempo outBand</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">% outband</th>";
                        html += "</tr>";
                        html += "</table>";
                        html += "</td>";
                    }
                    html += "<td>";
                    html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                    html += "<tr>";
                    html += "<th class=\"spec\">" + Sesion.getVariables().get(i).getAbrevi() + "</th>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMax()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMin()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getProm()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getDesv()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getCantOutBand()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getOutBanda()) + "</td>";
                    html += "</tr>";
                    html += "</table>";
                    html += "</td>";
                    a++;
                }
            }
            html += "</tr>";
            html += "<tr>";
            html += "<td height=\"20\">&nbsp</td>";
            html += "</tr>";
            html += "<tr>";
            a = 0;
            for (int i = 0; i < trans.getEstadisticas().size(); i++) {
                ModEstadistica estadis = trans.getEstadisticas().get(i);
                if (estadis.getIdEsta() == 200 || estadis.getIdEsta() == 201 || estadis.getIdEsta() == 202) {
                    if (a == 0) {
                        html += "<td>";
                        html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                        html += "<tr>";
                        html += "<th class=\"nobg\">&nbsp</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Max</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Min</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Prom</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Varia</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Tiempo outBand</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">% outband</th>";
                        html += "</tr>";
                        html += "</table>";
                        html += "</td>";
                    }
                    html += "<td>";
                    html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                    html += "<tr>";
                    html += "<th class=\"spec\">" + Sesion.getVariables().get(i).getAbrevi() + "</th>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMax()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMin()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getProm()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getDesv()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getCantOutBand()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getOutBanda()) + "</td>";
                    html += "</tr>";
                    html += "</table>";
                    html += "</td>";
                    a++;
                }
            }
            html += "<td width=\"20\">&nbsp</td>";
            for (int i = 0; i < trans.getEstadisticas().size(); i++) {
                ModEstadistica estadis = trans.getEstadisticas().get(i);
                if (estadis.getIdEsta() == 300 || estadis.getIdEsta() == 301 || estadis.getIdEsta() == 302) {
                    if (a == 0) {
                        html += "<td>";
                        html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                        html += "<tr>";
                        html += "<th class=\"nobg\">&nbsp</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Max</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Min</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Prom</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Varia</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Tiempo outBand</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">% outband</th>";
                        html += "</tr>";
                        html += "</table>";
                        html += "</td>";
                    }
                    html += "<td>";
                    html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                    html += "<tr>";
                    html += "<th class=\"spec\">" + Sesion.getVariables().get(i).getAbrevi() + "</th>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMax()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMin()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getProm()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getDesv()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getCantOutBand()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getOutBanda()) + "</td>";
                    html += "</tr>";
                    html += "</table>";
                    html += "</td>";
                    a++;
                }
            }
            html += "<td width=\"20\">&nbsp</td>";
            for (int i = 0; i < trans.getEstadisticas().size(); i++) {
                ModEstadistica estadis = trans.getEstadisticas().get(i);
                if (estadis.getIdEsta() == 400 || estadis.getIdEsta() == 401 || estadis.getIdEsta() == 202) {
                    if (a == 0) {
                        html += "<td>";
                        html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                        html += "<tr>";
                        html += "<th class=\"nobg\">&nbsp</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Max</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Min</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Prom</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Varia</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">Tiempo outBand</th>";
                        html += "</tr>";
                        html += "<tr>";
                        html += "<th class=\"spec\">% outband</th>";
                        html += "</tr>";
                        html += "</table>";
                        html += "</td>";
                    }
                    html += "<td>";
                    html += "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">";
                    html += "<tr>";
                    html += "<th class=\"spec\">" + Sesion.getVariables().get(i).getAbrevi() + "</th>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMax()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getMin()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getProm()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getDesv()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getCantOutBand()) + "</td>";
                    html += "</tr>";
                    html += "<tr>";
                    html += "<td class=\"estil\">" + nf.format(estadis.getOutBanda()) + "</td>";
                    html += "</tr>";
                    html += "</table>";
                    html += "</td>";
                    a++;
                }
            }
            html += "<td width=\"400\">";
            html += "<table  width=\"90%\">";
            html += "<tr>";
            html += "<td align=\"right\" valign=\"bottom\" height=\"130\">";
            html += "<a href=\"Main.do?mnu=requerimientos\">";
            html += "<img alt=\"\" name=\"volver\" id=\"volver\" class=\"boton\" src=\"img/images/volver.jpg\" border=\"0\"/>";
            html += "</a>";
            html += "</td>";
            html += "</tr>";
            html += "</table>";
            html += "</td>";
            html += "</tr></table>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public static String registrosDisponibles(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            ArrayList<ModVariable> variables = Sesion.getVariables();
            for (int i = 0; i < variables.size(); i++) {
                ModVariable variable = variables.get(i);
                html += " <tr> ";
                html += " <td> ";
                if (variable.isEstado()) {
                    html += " <input type=\"checkbox\" id=\"variable" + i + "\" name=\"variable" + i + "\" value=\"checkbox\" checked=\"checked\" /> ";
                } else {
                    html += " <input type=\"checkbox\" id=\"variable" + i + "\" name=\"variable" + i + "\" value=\"checkbox\" /> ";
                }
                html += variable.getDescripcion();
                html += " </td> ";
                html += " </tr> ";
                variable = null;
            }
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public static String CargarGrafico(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            for (int i = 0; i < Sesion.getVariables().size(); i++) {
                if (req.getParameter("variable" + i) != null) {
                    html += " <script type=\"text/javascript\"> ";
                    html += " function contar() { ";
                    html += " alert('" + req.getParameter("variable" + i) + "')";
                    html += " } ";
                    html += " </script> ";
                }
            }
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public static String menuTemplate(HttpServletRequest request, String mnu) {
        String html = "";
        ModSesion Sesion = null;

        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            html += "<ul>" + "\n";
            for (int i = 0; i < Sesion.getListadoMenu().size(); i++) {
                String dato = (String) Sesion.getListadoMenu().get(i);
                if (dato.split("-")[2].equals("si") && Sesion.isEnSesion()) {
                    html += "<li><a href=\"" + Sesion.getTemplate() + dato.split("-")[1] + "\">" + dato.split("-")[0] + "</a></li>" + "\n";
                } else if (dato.split("-")[2].equals("no") && !Sesion.isEnSesion()) {
                    html += "<li><a href=\"" + Sesion.getTemplate() + dato.split("-")[1] + "\">" + dato.split("-")[0] + "</a></li>" + "\n";
                }
            }
            html += "</ul>" + "\n";
        } catch (Exception ex) {
            html = "<tr class='Paginador'>\n" + "<td colspan='8' align='center'>";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaTransformadores(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            ArrayList<ModTransformador> trans = Sesion.getUsuario().getEmpresa().getTransformadores();
            int ri = 0;
            int rt = 0;

            if (trans.isEmpty()) {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='7' align='center' class=\"ConBorder\">";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            } else {
                for (int i = 0; i < trans.size(); i++) {
                    int count = 0;
                    ModTransformador transfo = (ModTransformador) Sesion.getUsuario().getEmpresa().getTransformadores().get(i);
                    if (!transfo.getFallas().isEmpty()) {
                        for (int a = 0; a < transfo.getFallas().size(); a++) {
                            count++;
                            ModFalla falla = (ModFalla) transfo.getFallas().get(a);
                            html += "<tr>\n";
                            html += "<td align=\"center\" width=\"40px\" " + verOverLib("<b> Id Trasnformador: </b><br/>" + transfo.getIdTrans()) + "align='left'>" + transfo.getIdTrans() + "</td>\n";
                            html += "<td align=\"center\" width=\"250px\" " + verOverLib("<b> Descripci�n: </b><br/>" + transfo.getDescripcion()) + "align='left'>" + verHtmlNull(transfo.getDescripcion()) + "</td>\n";
                            html += "<td align=\"center\" width=\"60px\" " + verOverLib("<b> Serie: </b><br/>" + transfo.getSerie()) + "align='left'>" + verHtmlNull(transfo.getSerie()) + "</td>\n";
                            html += "<td align=\"center\" width=\"100px\" " + verOverLib("<b> F�brica: </b><br/>" + transfo.getFabricante()) + "align='left'>" + verHtmlNull(transfo.getFabricante()) + "</td>\n";
                            html += "<td align=\"center\" width=\"80px\" " + verOverLib("<b> Fabricaci�n: </b><br/>" + transfo.getFecha()) + "align='left'>" + verHtmlNull(transfo.getFecha()) + "</td>\n";
                            html += "<td align=\"center\" width=\"120px\" " + verOverLib("<b> �ltima lectura: </b><br/>" + transfo.getEstadisticas().get(0).getUltimaActu()) + "align='left'>" + transfo.getEstadisticas().get(0).getUltimaActu() + "</td>\n";
                            html += " <td align=\"center\" width=\"80px\" class=\"" + falla.getColor() + "\">";
                            //html += " <a href='Main.do?mnu=requerimientos&frm=registros&descriFalla=" + falla.getDescripcion() + "&fecha=" + falla.getLectura() + "&dias=" + falla.getDias() + "&estFalla=" + falla.getIdEstado() + "&falla=" + falla.getIdFalla() + "&posicion=" + i + "' ";
                            html += " <a href='Main.do?mnu=requerimientos&frm=registros&falla=" + a + "&transfo=" + i + "' ";
                            html += verOverLib("<b> Nombre falla: </b><br/>" + falla.getNombre()) + " ";
                            html += " style='text-decoration: underline;' onclick=\"javascript:loading();\">";
                            html += falla.getNombre();
                            html += " </a></td>\n";
                            html += " <td align=\"center\" width=\"60px\">&nbsp;0,341&nbsp;</td>\n ";
                            html += " <td align=\"center\" width=\"80px\">&nbsp;0,245&nbsp;</td>\n ";
                            html += " <td align=\"center\" width=\"80px\">&nbsp;0,272&nbsp;</td>\n ";
                            html += " <td align=\"center\" width=\"0px\">&nbsp;3&nbsp;</td>\n ";
                            html += "</tr>";
                        }
                    } else {
                        html += "<tr>\n";
                        html += "<td align=\"center\" width=\"40px\" " + verOverLib("<b> Id Trasnformador: </b><br/>" + transfo.getIdTrans()) + "align='left'>" + transfo.getIdTrans() + "</td>\n";
                        html += "<td align=\"center\" width=\"250px\" " + verOverLib("<b> Descripci�n: </b><br/>" + transfo.getDescripcion()) + "align='left'>" + verHtmlNull(transfo.getDescripcion()) + "</td>\n";
                        html += "<td align=\"center\" width=\"60px\" " + verOverLib("<b> Serie: </b><br/>" + transfo.getSerie()) + "align='left'>" + verHtmlNull(transfo.getSerie()) + "</td>\n";
                        html += "<td align=\"center\" width=\"100px\" " + verOverLib("<b> F�brica: </b><br/>" + transfo.getFabricante()) + "align='left'>" + verHtmlNull(transfo.getFabricante()) + "</td>\n";
                        html += "<td align=\"center\" width=\"80px\" " + verOverLib("<b> Fabricaci�n: </b><br/>" + transfo.getFecha()) + "align='left'>" + verHtmlNull(transfo.getFecha()) + "</td>\n";
                        if (!transfo.getEstadisticas().isEmpty()) {
                            html += "<td align=\"center\" width=\"120px\"" + verOverLib("<b> �ltima lectura: </b><br/>" + transfo.getEstadisticas().get(0).getUltimaActu()) + "align='left'>" + transfo.getEstadisticas().get(0).getUltimaActu() + "</td>\n";
                        } else {
                            html += "<td align=\"center\" width=\"120px\"" + verOverLib("<b> �ltima lectura: </b><br/> Sin lectura") + " align='left'>Sin lectura</td>\n";
                        }
                        html += " <td align=\"center\" width=\"80px\">";
                        html += " Sin Falla ";
                        html += " </td>\n";
                        html += " <td align=\"center\" width=\"60px\">&nbsp;0,341&nbsp;</td>\n ";
                        html += " <td align=\"center\" width=\"80px\">&nbsp;0,245&nbsp;</td>\n ";
                        html += " <td align=\"center\" width=\"80px\">&nbsp;0,272&nbsp;</td>\n ";
                        html += " <td align=\"center\" width=\"0px\">&nbsp;3&nbsp;</td>\n ";
                        html += "</tr>";

                    }
                    transfo = null;
                }
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='7' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaTransformadoresRed(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            if (Sesion.getUsuario().getEmpresa().getTransformadoresRed().isEmpty()) {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='7' align='center' class=\"ConBorder\">";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            } else {
                for (int i = 0; i < Sesion.getUsuario().getEmpresa().getTransformadoresRed().size(); i++) {
                    ModTransformador transfo = (ModTransformador) Sesion.getUsuario().getEmpresa().getTransformadoresRed().get(i);
                    html += "<tr>\n";
                    html += "<td align=\"center\" width=\"40px\">";
                    html += " <a href='Main.do?frm=setearDatos&idTransformador=" + i + "' ";
                    html += verOverLib("<b> Id Trasnformador: </b><br/>" + transfo.getIdTrans()) + " ";
                    html += " style='text-decoration: underline;' onclick=\"javascript:loading();\">";
                    html += transfo.getIdTrans();
                    html += " </a></td>\n";
                    //html += "<td align=\"center\" width=\"40px\" " + verOverLib("<b> Id Trasnformador: </b><br/>" + transfo.getIdTrans()) + "align='left'>" + transfo.getIdTrans() + "</td>\n";
                    html += "<td align=\"center\" width=\"250px\" " + verOverLib("<b> Descripci�n: </b><br/>" + transfo.getDescripcion()) + "align='left'>" + verHtmlNull(transfo.getDescripcion()) + "</td>\n";
                    html += "<td align=\"center\" width=\"60px\" " + verOverLib("<b> Serie: </b><br/>" + transfo.getSerie()) + "align='left'>" + verHtmlNull(transfo.getSerie()) + "</td>\n";
                    html += "<td align=\"center\" width=\"100px\" " + verOverLib("<b> F�brica: </b><br/>" + transfo.getFabricante()) + "align='left'>" + verHtmlNull(transfo.getFabricante()) + "</td>\n";
                    html += "<td align=\"center\" width=\"80px\" " + verOverLib("<b> Fabricaci�n: </b><br/>" + transfo.getFecha()) + "align='left'>" + verHtmlNull(transfo.getFecha()) + "</td>\n";
                    if (!transfo.getEstadisticas().isEmpty()) {
                        html += "<td align=\"center\" width=\"120px\"" + verOverLib("<b> �ltima lectura: </b><br/>" + transfo.getEstadisticas().get(0).getUltimaActu()) + "align='left'>" + transfo.getEstadisticas().get(0).getUltimaActu() + "</td>\n";
                    } else {
                        html += "<td align=\"center\" width=\"120px\"" + verOverLib("<b> �ltima lectura: </b><br/> Sin lectura") + " align='left'>Sin lectura</td>\n";
                    }
                    html += " <td align=\"center\" width=\"80px\">";
                    html += " Sin Falla ";
                    html += " </td>\n";
                    html += " <td align=\"center\" width=\"60px\">&nbsp;0,341&nbsp;</td>\n ";
                    html += " <td align=\"center\" width=\"80px\">&nbsp;0,245&nbsp;</td>\n ";
                    html += " <td align=\"center\" width=\"80px\">&nbsp;0,272&nbsp;</td>\n ";
                    html += " <td align=\"center\" width=\"0px\">&nbsp;3&nbsp;</td>\n ";
                    html += "</tr>";
                    transfo = null;
                }
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='7' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String estadoFallaSolucionada(boolean est, byte estado, String valor) {
        String html = "";
        try {
            if (est) {
                html += "<tr>";
                if (estado == 3) {
                    html += "<td align=\"center\" bgcolor=\"#009966\" width=\"90%\">";
                } else {
                    html += "<td align=\"center\" bgcolor=\"#FFFF99\" width=\"90%\">";
                }
                html += valor + ".";
                html += "</td>";
                html += "</tr>";
                html += "<tr>";
                html += "<td height=\"30px\">&nbsp;</td>";
                html += "</tr>";
                html += "<tr>";
                html += "<td align=\"center\" width=\"90%\">";
                html += "<img alt=\"\" name=\"cerrar\" id=\"cerrar\" onclick='cerrarPopUp();' src=\"img/btn40x40/salir.JPG\" border=\"0\" />";
                html += "</td>";
                html += "</tr>";
            } else {
                html += "<tr>";
                html += "<td align=\"center\" bgcolor=\"#FF3333\" width=\"90%\" colspan=\"2\">";
                html += valor + ".";
                html += "</td>";
                html += "</tr>";
                html += "<tr>";
                html += "<td height=\"30px\" colspan=\"2\">&nbsp;</td>";
                html += "</tr>";
                html += " <tr> ";
                html += " <td> ";
                html += " Observaci�n:";
                html += " </td> ";
                html += " </tr> ";
                html += " <tr> ";
                html += " <td width=\"90%\"  colspan=\"2\"> ";
                html += " <textarea name=\"obs\" id=\"obs\" cols=\"42\" rows=\"2\"></textarea> ";
                html += " </td> ";
                html += " </tr>";
                html += " <tr>";
                html += " <td align=\"center\">";
                html += " <img alt=\"\" name=\"cerrar\" id=\"cerrar\" onclick='cerrarPopUp();' src=\"img/btn40x40/salir.JPG\" border=\"0\" />";
                html += " </td> ";
                html += " <td>";
                html += " <img alt=\"\" name=\"actualizar\" id=\"actualizar\" src=\"img/images/actualizar.JPG\" border=\"0\" onclick=\"cambiarEstado();\"/>";
                html += " </td>";
                html += " </tr>";
            }


        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='7' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String descripcionFalla(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html += " <table  width=\"100%\" cellpadding=\"0\" cellspacing=\"6\" class=\"Contenido\"> ";
            html += " <tr>";
            html += " <td valign=\"bottom\" class=\"sinBorde\">";
            html += "<font class=\"data\">" + Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getDescripcion() + "</font>";
            html += " </td>";
            html += " </tr>";
            html += " <tr>";
            html += " <td class=\"sinBorde\"><font class=\"data\">Voltaje m�ximo mas reciente:</font> <font class=\"data1\">" + Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getMax() + " " + Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getAbrev() + "</font></td>";
            html += " </tr>";
            html += " <tr>";
            html += " <td class=\"sinBorde\">";
            html += "<font class=\"data\">El&nbsp;</font> <font class=\"data1\">" + Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getLectura().toString().substring(0, 10) + "</font>&nbsp;&nbsp;";
            html += "<font class=\"data\">A Las </font>&nbsp;<font class=\"data1\">" + Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getLectura().toString().substring(11, Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getLectura().toString().length() - 2) + "</font>";
            html += " </td>";
            html += " </tr>";
            html += " </table>";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='7' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String datosTrans(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html += Sesion.getUsuario().getEmpresa().getTransformador().getDescripcion();
            html += "&nbsp&nbsp&nbsp" + Sesion.getUsuario().getEmpresa().getTransformador().getModelo();
            html += "&nbsp&nbsp&nbsp" + Sesion.getUsuario().getEmpresa().getTransformador().getSerie();
            html += "&nbsp&nbsp&nbsp" + Sesion.getUsuario().getEmpresa().getTransformador().getFabricante();
            html += "&nbsp&nbsp&nbsp" + Sesion.getUsuario().getEmpresa().getTransformador().getFecha();

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='7' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String grillaBitacoraFalla(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            ArrayList<ModBitacoraFalla> bitacora = Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getBitacora();
            for (int i = 0; i < bitacora.size(); i++) {
                ModBitacoraFalla bita = (ModBitacoraFalla) bitacora.get(i);
                html += "<table>";
                html += "<tr>";
                html += "<td align=\"center\" bgcolor=\"#FF3333\" width=\"90%\">";
                html += bita.getObservacion();
                html += "</td>";
                html += "<td align=\"center\" bgcolor=\"#FF3333\" width=\"90%\">";
                html += bita.getFecha();
                html += "</td>";
                html += "<td align=\"center\" bgcolor=\"#FF3333\" width=\"90%\">";
                html += bita.getUsuario();
                html += "</td>";
                html += "</tr>";
            }


        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='7' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String cargarAllFallas(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            ArrayList<ModFalla> fallas = Sesion.getUsuario().getEmpresa().getAllFallas();
            for (int i = 0; i < fallas.size(); i++) {
                ModFalla falla = (ModFalla) fallas.get(i);
                html += " <tr> ";
                html += " <td align=\"center\">";
                if (Sesion.getUsuario().getEmpresa().getTransformador().getIdTrans() > 0) {
                    html += " <a href=\"Main.do?frm=setearDatos&idFalla=" + i + "\" style='text-decoration: underline;' onclick=\"javascript:loading();\">" + falla.getNombre() + "</a>";
                } else {
                    html += falla.getNombre();
                }
                html += " </td> ";
                html += " </tr>";
            }

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='2' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String cargarRedesEmpresa(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            ArrayList<ModRed> redes = Sesion.getUsuario().getEmpresa().getRedes();
            for (int i = 0; i < redes.size(); i++) {
                ModRed red = (ModRed) redes.get(i);
                html += " <tr>";
                html += " <td align=\"center\">";
                html += " <a href=\"Main.do?frm=setearDatos&idRed=" + i + "\" style='text-decoration: underline;' onclick=\"javascript:loading();\">" + red.getNombre() + "</a>";
                html += " </td> ";
                html += " </tr>";
            }

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='2' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public static String comboAnalisisTime(HttpServletRequest req, double valor, String nombre) {
        String html = null;
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            html = "<select name=\"" + nombre + "\" id=\"" + nombre + "\">";
            html += "<option value=\"\">Par�metro</option>";
            for (int i = 0; i < Sesion.getAnalisisTiempo().size(); i++) {
                ModParametro param = (ModParametro) Sesion.getAnalisisTiempo().get(i);
                if (param.getIdParam() == valor) {
                    html += "<option value=\"" + param.getIdParam() + "\" selected>" + param.getNombre() + "</option>";
                } else {
                    html += "<option value=\"" + param.getIdParam() + "\">" + param.getNombre() + "</option>";
                }
            }
            html += "</select>";
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return html;
    }

    public String cargarParametroSeteado(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        String origen = "";
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            ModParametro param = Sesion.getUsuario().getEmpresa().getParametro();
            html += " <tr> ";
            html += " <td width=\"80\" class=\"color5\" height=\"30\">Valor actual</td> ";
            if (Sesion.getAnalisisTiempo().isEmpty()) {
                html += " <td width=\"290\" class=\"alto1 color5\">" + param.getMax() + "&nbsp " + param.getUnidad() + " </td> ";
            } else {
                html += " <td width=\"290\" class=\"alto1 color5\">" + PrmRegistro.tiempoAnalisis(param.getMax()) + " </td> ";
            }

            html += " <td width=\"230\" class=\"color5\">Estandar&nbsp" + param.getArea() + " </td> ";
            html += " </tr>";
            html += " <tr> ";
            html += " <td colspan=\"3\" class=\"color6\"> ";
            html += " <table width=\"500\" cellpadding=\"0\" cellspacing=\"0\"> ";
            html += " <tr> ";
            if (Sesion.getAnalisisTiempo().isEmpty()) {
                if (param.getArea().equals("trafonet")) {
                    html += " <td  align=\"center\" height=\"50\" class=\"color6\"><input id=\"estandartrafonetv\" name=\"estandartrafonetv\" type=\"text\" size=\"5\" value=\"" + param.getMaxTrafonet() + "\" readonly/></td> ";
                } else {
                    html += " <td  align=\"center\" height=\"50\"><input id=\"estandartrafonetv\" name=\"estandartrafonetv\" type=\"text\" size=\"5\" value=\"" + param.getMaxTrafonet() + "\" readonly/></td> ";
                }
                if (param.getMaxEmpre() == 0) {
                    html += " <td align=\"center\"><input id=\"estandarempresav\" name=\"estandarempresav\" type=\"text\" size=\"5\" value=\"\"/></td> ";
                } else {
                    if (param.getArea().equals("empresa")) {
                        html += " <td align=\"center\" class=\"color6\"><input id=\"estandarempresav\" name=\"estandarempresav\" type=\"text\" size=\"5\" value=\"" + param.getMaxEmpre() + "\"/></td> ";
                    } else {
                        html += " <td align=\"center\" class=\"color4\"><input id=\"estandarempresav\" name=\"estandarempresav\" type=\"text\" size=\"5\" value=\"" + param.getMaxEmpre() + "\"/></td> ";
                    }
                }
                if (param.getMaxTrans() == 0) {
                    html += " <td align=\"center\"><input id=\"estandarequipov\" name=\"estandarequipov\" type=\"text\" size=\"5\" value=\"\"/></td> ";
                } else {
                    if (param.getArea().equals("equipo")) {
                        html += " <td align=\"center\" class=\"color6\"><input id=\"estandarequipov\" name=\"estandarequipov\" type=\"text\" size=\"5\" value=\"" + param.getMaxTrans() + "\"/></td> ";
                    } else {
                        html += " <td align=\"center\" class=\"color4\"><input id=\"estandarequipov\" name=\"estandarequipov\" type=\"text\" size=\"5\" value=\"" + param.getMaxTrans() + "\"/></td> ";
                    }
                }
            } else {
                if (param.getArea().equals("trafonet")) {
                    html += " <td  align=\"center\" height=\"50\" class=\"color6\" readonly>";
                    html += comboAnalisisTime(req, param.getMaxTrafonet(), "estandartrafonetv");
                    html += " </td> ";
                } else {
                    html += " <td  align=\"center\" height=\"50\" readonly>";
                    html += comboAnalisisTime(req, param.getMaxTrafonet(), "estandartrafonetv");
                    html += " </td> ";
                }
                if (param.getMaxEmpre() == 0) {
                    html += " <td  align=\"center\" height=\"50\">";
                    html += comboAnalisisTime(req, -1, "estandarempresav");
                    html += " </td> ";
                } else {
                    if (param.getArea().equals("empresa")) {
                        html += " <td  align=\"center\" height=\"50\" class=\"color6\">";
                        html += comboAnalisisTime(req, param.getMaxEmpre(), "estandarempresav");
                        html += " </td> ";
                    } else {
                        html += " <td  align=\"center\" height=\"50\">";
                        html += comboAnalisisTime(req, param.getMaxEmpre(), "estandarempresav");
                        html += " </td> ";
                    }
                }
                if (param.getMaxTrans() == 0) {
                    html += " <td  align=\"center\" height=\"50\" class=\"color6\">";
                    html += comboAnalisisTime(req, -1, "estandarequipov");
                    html += " </td> ";
                } else {
                    if (param.getArea().equals("equipo")) {
                        html += " <td  align=\"center\" height=\"50\" class=\"color6\">";
                        html += comboAnalisisTime(req, param.getMaxTrans(), "estandarequipov");
                        html += " </td> ";
                    } else {
                        html += " <td  align=\"center\" height=\"50\" class=\"color4\">";
                        html += comboAnalisisTime(req, param.getMaxTrans(), "estandarequipov");
                        html += " </td> ";
                    }
                }
            }
            html += " </tr> ";
            html += " <tr> ";
            if (param.getArea().equals("trafonet")) {
                html += " <td align=\"center\"><input id=\"estandartrafonet\" name=\"estandartrafonet\" type=\"submit\" class=\"boton10\" value=\"Estandar Trafonet\" disabled/></td> ";
            } else {
                html += " <td align=\"center\"><input id=\"estandartrafonet\" name=\"estandartrafonet\" type=\"submit\" class=\"boton10\" value=\"Estandar Trafonet\"/></td> ";
            }
            html += " <td align=\"center\"><input id=\"estandarempresa\" name=\"estandarempresa\" type=\"submit\" class=\"boton10\" value=\"Estandar Empresa\" /></td> ";
            html += " <td align=\"center\"><input id=\"estandarequipo\" name=\"estandarequipo\" type=\"submit\" class=\"boton10\" value=\"Estandar Equipo\" /></td> ";


            html += " </tr> ";
            html += " </table> ";
            html += " </td> ";
            html += " </tr> ";


        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='2' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String cargarParametros(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            ArrayList<ModParametro> parametros = Sesion.getUsuario().getEmpresa().getParametros();
            for (int i = 0; i < parametros.size(); i++) {
                ModParametro param = (ModParametro) parametros.get(i);
                html += " <tr> ";
                html += "<td align=\"left\" width=\"179\" class=\"link\">";
                html += " <a href='Main.do?frm=setearDatos&idParametros=" + i + "' ";
                html += verOverLib("<b> Id Parametro: </b><br/>" + param.getIdParam()) + " ";
                html += " onclick=\"javascript:loading();\">";
                html += param.getDescripcion();
                html += " </a></td>\n";
                //html += " <td>" + param.getDescripcion() + "</td> ";
                html += " <td width=\"49\" align=\"center\">" + param.getTipo() + "</td> ";
                html += " <td width=\"185\" align=\"center\">" + param.getFecha() + "</td> ";
                html += " <td width=\"59\" align=\"center\">" + param.getUsuario() + "</td> ";
                html += " <td width=\"99\" align=\"center\">" + param.getMax() + "</td> ";
                html += " <td width=\"99\" align=\"center\">" + param.getArea() + "</td> ";
                html += " </tr>";
                param = null;
            }

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='2' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }

    public String cargarCumplimientosVoltajes(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        DecimalFormat format = new DecimalFormat(".00");
        html += " <tr> ";
        html += " <td> ";
        html += " <table border=\"1\"> ";
        html += " <tr> ";
        html += " <td colspan=\"4\" align=\"center\"> Voltaje referido a BT </td>";
        html += " </tr> ";
        html += " <tr> ";
        html += " <td> Limite inferior </td>";
        html += " <td> Nominal </td>";
        html += " <td> Limite superior </td>";
        html += " <td> Tiempo cumplimiento </td>";
        html += " </tr> ";
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            ArrayList<ModEstadistica> estadis = Sesion.getUsuario().getEmpresa().getTransformador().getEstadisticas();

            for (int i = 0; i < estadis.size(); i++) {
                ModEstadistica esta = (ModEstadistica) estadis.get(i);
                if (esta.getIdEsta() == 0 || esta.getIdEsta() == 1 || esta.getIdEsta() == 2) {
                    html += " <tr> ";
                    html += " <td align=\"center\">" + esta.getMinLim() + "</td>";
                    html += " <td align=\"center\">" + 0 + "</td>";
                    html += " <td align=\"center\">" + esta.getMaxLim() + "</td>";
                    html += " <td align=\"center\">" + format.format(100 - esta.getTiempoCumplimiento()) + "%</td>";
                    html += " </tr>";
                }
            }

            html += " </table> ";
            html += " </td> ";
            html += " </tr> ";

        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='4' align='center' class=\"ConBorder\">";
            PrmApp.addError(ex, true);
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
}
