package action;

import controllers.PrmApp;
import controllers.PrmDato;
import model.ModSesion;
import javax.servlet.http.*;
import model.apl.ModTransformador;
import org.apache.struts.action.*;

public class ActionApp extends Action {

    /**
     * Metodo de control a las solicitudes <b>HTTP</b> a la <b>URL<b>: <i>Main.do</i>.
     * este metodo inicializa la conexion con las bases de datos, el idioma inicial asi como la sesion de usuario.<br>
     * tambien controla los procesos de cambio de idioma y movimiento en el menu principal.
     *
     * @since JDK 1.4.1
     * @param amap Control del mapa de accion
     * @param afrm Control del formulario de accion
     * @param req Datos enviados por formulario a la clase
     * @param res Informacion a responder al controlador Servlet
     * @return control de enlace y vinculacion Struts
     */
    @Override
    public ActionForward execute(ActionMapping amap, ActionForm afrm, HttpServletRequest req, HttpServletResponse res) {
        String body = "link";
        ModSesion Sesion = null;
        try {
            if (req.getSession().getAttribute("Sesion") != null) {
                Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
                /** VALIDACION DE SESION DE USUARIO ACTIVA **/
                //--[recuperacion de nombre de controlador de formulario]
                if (req.getParameter("frm") != null) {
                    body = req.getParameter("frm");
                }
                //--[lectura de opcion del menu]
                if (req.getParameter("mnu") != null) {
                    String httpPage = req.getParameter("mnu");
                    if (httpPage.equals(".logout")) {
                        Sesion = new ModSesion();
                    } else {
                        Sesion.setHttp_Body(httpPage);
                    }
                }


                if (Sesion.getVariables().isEmpty()) {
                    Sesion.setVariables(PrmDato.cargarVariables());
                }
                if (Sesion.getHttp_Body().equals("home")) {
                    for (int i = 0; i < Sesion.getUsuario().getEmpresa().getTransformadores().size(); i++) {
                        ModTransformador transfo = (ModTransformador) Sesion.getUsuario().getEmpresa().getTransformadores().get(i);
                        transfo.setFallas(PrmDato.cargarFallas(transfo.getIdTrans()));
                        //transfo.setEstadisticasFalla1(PrmDato.cargarEstadisticasTransformadoresPorFalla1(transfo.getIdTrans()));
                    }
                }


            } else {
                Sesion = new ModSesion();
            }
            req.getSession().setAttribute("Sesion", Sesion);
        } catch (Exception ex) {
            Sesion.setTip("Error General al Procesar Informacion.<br> contactese con su administrador de servicios.", 1);
            PrmApp.addError(ex, true);
        }
        /** TRASPASO DE OBJETOS GLOBALES DE EJECUCION **/
        //{Http_Body}	-> control de la pantalla a visualizar "ver tiles-def.xml".
        //{LOAD_ONSUBMIT}	-> String con script para que formulario visualicen mensaje "cargando".
        //{LOAD_ONCLICK}	-> String con script para que los <a href> visualicen mensaje "cargando".
        req.setAttribute("LOAD_ONSUBMIT", "onsubmit=\"javascript:loading();\"");
        req.setAttribute("LOAD_ONCLICK", "onclick=\"javascript:loading();\"");
        /**REDIRECCIONAMIENTO A CONTROLADOR DE FORMULARIO**/
        return amap.findForward(body);
    }
}
