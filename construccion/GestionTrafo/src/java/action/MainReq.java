package action;

import controllers.PrmApp;
import model.ModSesion;
import javax.servlet.http.*;
import org.apache.struts.action.*;


public class MainReq extends Action {

    public ModSesion Sesion = null;

    public void cargarObj_Sesion(HttpServletRequest req) {
        Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
    }

    public void SendtoHome(HttpServletResponse res, HttpServletRequest req) {
        cargarObj_Sesion(req);
        try {
            String lnk = "mnu=" + Sesion.getHttp_Body();
            res.sendRedirect(Sesion.getLinkDO() + "?" + lnk);
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
    }

    public void LimpiarSesiones(HttpServletRequest req, boolean full) throws Exception {
        if (full) {
            req.getSession().setAttribute("Sesion", Sesion = new ModSesion());
        } else {

            req.getSession().setAttribute("Sesion", Sesion);
        }
    }

    public static String processException(Exception e) {
        String Err = "";
        StackTraceElement nn[] = e.getStackTrace();
        Err = "<b>ERROR : " + e.toString() + "</b>" + "<br>";
        for (int i = 0; i < nn.length; i++) {
            Err = Err + nn[i].toString() + "<br>";
        }
        return Err;
    }
}
