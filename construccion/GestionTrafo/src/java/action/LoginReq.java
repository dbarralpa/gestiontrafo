package action;

import controllers.PrmApp;
import controllers.PrmDato;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ModSesion;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class LoginReq extends MainReq {

    
    public ActionForward execute(ActionMapping amap, ActionForm afrm, HttpServletRequest req, HttpServletResponse res) {
        /** INICIO DE SESION **/
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");

            if (req.getParameter("Submit.x") != null) {
                LimpiarSesiones(req, true);
                String user = req.getParameter("txtUsr");
                String pass = req.getParameter("txtPwd");

                if (!user.equals("") && !pass.equals("")) {
                    String consulta = PrmDato.autenticaUsuario(user, pass);
                    if (consulta.equals("VIGENT")) {
                        Sesion.setUsuario(PrmDato.cargarUsuario(user, pass));
                        Sesion.setEnSesion(true);
                    } else if (consulta.equals("ELIMIN")) {
                        Sesion.setTip("Usuario Eliminado", 0);
                    } else if (consulta.equals("NOVIGE")) {
                        Sesion.setTip("Usuario No Vigente", 0);
                    } else {
                        Sesion.setTip("Datos incorrectos", 0);
                    }
                } else {
                    Sesion.setTip("Los campos Usuario y Clave no pueden estar vacios", 0);
                }
            }
            if (req.getParameter("LogOut.x") != null) {
                LimpiarSesiones(req, true);
            }
            req.getSession().setAttribute("Sesion", Sesion);
        } catch (Exception ex) {
            Sesion.setTip("Error General al Procesar Informacion.<br> contactese con su administrador de servicios.", 1);
            PrmApp.addError(ex, true);            
        }
        super.SendtoHome(res, req);
        return null;
    }
}
