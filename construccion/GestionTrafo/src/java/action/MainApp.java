package action;

import model.ModSesion;
import java.io.*;
import javax.servlet.http.*;
import javax.servlet.*;
import org.apache.struts.action.*;
import org.apache.struts.tiles.*;
import org.apache.struts.tiles.actions.*;


public class MainApp extends TilesAction {

    /**
     * Metodo de control a las solicitudes <b>HTTP</b> a la plantilla <i>"ver struts-config.xml y tiles-def.xml"</i>.
     * este metodo valida la seccion a la cual estamos intentando accesar
     * y nos la visualiza en el formato estblecido por la plantilla <b><i>lpLayout.jsp</i></b>.
     *
     * @since JDK 1.4.1
     * @param cc Control de contexto del componente en ejecucion
     * @param amap Control de mapas de la accion
     * @param afrm Control de formularios de la accion
     * @param req Datos enviados por formulario desde Servlet
     * @param res Informacion de respuesta a controlador Servlet
     * @exception IOException Si no se pude acceder a escribir/leer informacion
     *     en el disco donde se ubica el contenedor
     * @exception ServletException Si el controlador de Servlet envia o
     *     recibe informacion en un formato incorrecto
     * @return Enlace de redireccionamiento a control Servlet o HTML
     *     correspondiente
     */
    
    public ActionForward execute(ComponentContext cc, ActionMapping amap, ActionForm afrm, HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        ModSesion Sesion = new ModSesion();
        /** LECTURA DE PARAMETROS PARA VALIDAR **/
        Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
        /** VALIDACION SI LA SECCION ESTA ACTIVA **/
        if (Sesion.isEnSesion()) {
            //--[si no existe pagina, se establece la principal]
            if (Sesion.getHttp_Body().equals("")) {
                Sesion.setHttp_Body("home");
            }
        } else {
            Sesion.setHttp_Body("home");
        }
        req.getSession().setAttribute("Sesion", Sesion);
        /** ESTABLECE LOS VALORES PARA VUSUALIZAR LA PAGINA **/
        cc.putAttribute("body", Sesion.getHttp_Body());
        return null;
    }
}
