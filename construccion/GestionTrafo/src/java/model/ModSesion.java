package model;

import java.util.ArrayList;
import model.apl.ModParametro;
import model.apl.ModVariable;

public class ModSesion {

    private String Http_Body = "home";
    private boolean enSesion = false;
    //** CONTROL DE ERRORES VIA MESSAGE BEAN **//
    private String tip = "";
    private int tipoTip = 0;
    private boolean conTip = false;
    private ArrayList registros;
    private ArrayList<ModVariable> variables;
    private ModUsuario usuario;
    private ModParametro parametro;
    private String guardarCarga;
    private String chk_PER_CODIGO = "checked";
    private String chk_PER_NOMBRE = "";
    private String template;
    private int cantRegistros;
    private int posiEmpre;
    private int posiTranfo;
    private int posiParam;
    private String cargarParteGrafico;
    private double min;
    private double max;
    private ArrayList listadoMenu;
    private ArrayList<ModParametro> parametros;
    private ArrayList<ModVariable> regSelected;
    private ArrayList<ModParametro> analisisTiempo;
    

    public ModSesion() {
        variables = new ArrayList<ModVariable>();
        usuario = new ModUsuario();
        registros = new ArrayList();
        guardarCarga = "";
        cantRegistros = 3000;
        parametro = new ModParametro();
        parametros = new ArrayList<ModParametro>();
        listadoMenu = new ArrayList();
        listadoMenu.add("Inicio-.jsp?url=login&logout=SI-no");
        listadoMenu.add("Inicio-.jsp?url=login&logout=SI-si");
        listadoMenu.add("Tecnología TRAFONET-.jsp?url=trafonet-no");
        listadoMenu.add("Tecnología TRAFONET-.jsp?url=trafonet-si");
        listadoMenu.add("Schaffner S.A.-.jsp?url=schaffner-si");
        listadoMenu.add("Schaffner S.A.-.jsp?url=schaffner-no");
        listadoMenu.add("Videos-.jsp?url=video-si");
        listadoMenu.add("Ayuda-.jsp?url=ayuda-no");
        listadoMenu.add("Ayuda-.jsp?url=ayuda-si");
        listadoMenu.add("Logout-.jsp?url=login&logout=SI-si");
        regSelected = new ArrayList();
        analisisTiempo = new ArrayList<ModParametro>();
        posiEmpre = -1;
        posiTranfo = -1;
        posiParam = -1;
        cargarParteGrafico = "";
        min = 1.1;
        max = 1.1;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

   

    public int getCantRegistros() {
        return cantRegistros;
    }

    public void setCantRegistros(int cantRegistros) {
        this.cantRegistros = cantRegistros;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public ArrayList<ModVariable> getRegSelected() {
        return regSelected;
    }

    public void setRegSelected(ArrayList<ModVariable> regSelected) {
        this.regSelected = regSelected;
    }
    public void setRegSelected(ModVariable regSelected) {
        this.regSelected.add(regSelected);
    }

    public ArrayList<ModParametro> getParametros() {
        return parametros;
    }

    public void setParametros(ArrayList<ModParametro> parametros) {
        this.parametros = parametros;
    }
    
    public ArrayList getListadoMenu() {
        return listadoMenu;
    }

    public void setListadoMenu(ArrayList listadoMenu) {
        this.listadoMenu = listadoMenu;
    }

    public ArrayList<ModVariable> getVariables() {
        return variables;
    }

    public void setVariables(ArrayList<ModVariable> variables) {
        this.variables = variables;
    }

    public String getGuardarCarga() {
        return guardarCarga;
    }

    public void setGuardarCarga(String guardarCarga) {
        this.guardarCarga = guardarCarga;
    }

    public ArrayList getRegistros() {
        return registros;
    }

    public void setRegistros(ArrayList registros) {
        this.registros = registros;
    }

    public String getChk_PER_CODIGO() {
        return chk_PER_CODIGO;
    }

    public void setChk_PER_CODIGO(String chk_PER_CODIGO) {
        this.chk_PER_CODIGO = chk_PER_CODIGO;
    }

    public String getChk_PER_NOMBRE() {
        return chk_PER_NOMBRE;
    }

    public void setChk_PER_NOMBRE(String chk_PER_NOMBRE) {
        this.chk_PER_NOMBRE = chk_PER_NOMBRE;
    }

    public String getLinkDO() {
        return "Main.do";
    }

    public String getEstadoSesion() {
        if (isEnSesion()) {
            return "login.ensesion";
        } else {
            return "login.iniciar";
        }
    }

    public String getNombreFull() {
        String nombreFull = "";
        if (getUsuario().getNombre() != null && getUsuario().getNombre().length() > 0) {
            nombreFull += getUsuario().getNombre().substring(0, 1);
        }
        if (getUsuario().getApellido() != null && getUsuario().getApellido().length() > 0) {
            nombreFull += "." + getUsuario().getApellido();
        }
        return nombreFull;
    }

    public String getHttp_Body() {
        return Http_Body;
    }

    public void setHttp_Body(String Http_Body) {
        this.Http_Body = Http_Body;
    }

    public boolean isEnSesion() {
        return enSesion;
    }

    public void setEnSesion(boolean enSesion) {
        this.enSesion = enSesion;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip, int tipoTip) {
        this.setTipoTip(tipoTip);
        this.setConTip(true);
        this.tip = tip;
    }

    public void cleanTip() {
        this.setTipoTip(0);
        this.setConTip(false);
        this.tip = "";
    }

    public boolean isConTip() {
        return conTip;
    }

    public void setConTip(boolean conTip) {
        this.conTip = conTip;
    }

    public int getTipoTip() {
        return tipoTip;
    }

    public void setTipoTip(int tipoTip) {
        this.tipoTip = tipoTip;
    }

    public ModUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(ModUsuario usuario) {
        this.usuario = usuario;
    }

    public int getPosiEmpre() {
        return posiEmpre;
    }

    public void setPosiEmpre(int posiEmpre) {
        this.posiEmpre = posiEmpre;
    }

    public int getPosiTranfo() {
        return posiTranfo;
    }

    public void setPosiTranfo(int posiTranfo) {
        this.posiTranfo = posiTranfo;
    }
    
    public String getCargarParteGrafico() {
        return cargarParteGrafico;
    }

    public void setCargarParteGrafico(String cargarParteGrafico) {
        this.cargarParteGrafico = cargarParteGrafico;
    }

    public int getPosiParam() {
        return posiParam;
    }

    public void setPosiParam(int posiParam) {
        this.posiParam = posiParam;
    }

    public ModParametro getParametro() {
        return parametro;
    }

    public void setParametro(ModParametro parametro) {
        this.parametro = parametro;
    }

    public ArrayList<ModParametro> getAnalisisTiempo() {
        return analisisTiempo;
    }

    public void setAnalisisTiempo(ArrayList<ModParametro> analisisTiempo) {
        this.analisisTiempo = analisisTiempo;
    }
}
