package model;

public class ModRol implements Cloneable{

    private int     id;
    private int     id_rol;    
    private String  nombre_rol;
    private String  desc_rol;
    private String  estado_rol;

    public ModRol(){
        id          = 0;
        id_rol      = 0;        
        nombre_rol  = "";
        desc_rol    = "";
        estado_rol  = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_rol() {
        return id_rol;
    }

    public void setId_rol(int id_rol) {
        this.id_rol = id_rol;
    }    

    public String getNombre_rol() {
        return nombre_rol;
    }

    public void setNombre_rol(String nombre_rol) {
        this.nombre_rol = nombre_rol;
    }

    public String getDesc_rol() {
        return desc_rol;
    }

    public void setDesc_rol(String desc_rol) {
        this.desc_rol = desc_rol;
    }

    public String getEstado_rol() {
        return estado_rol;
    }

    public void setEstado_rol(String estado_rol) {
        this.estado_rol = estado_rol;
    }

    public ModRol copia()
    {
        Object obj = null;
        try
        {
            obj = super.clone();
        }
        catch(CloneNotSupportedException ex) { }
        return (ModRol)obj;
    }
}
