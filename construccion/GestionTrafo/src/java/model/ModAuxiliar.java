package model;

import java.util.ArrayList;

public class ModAuxiliar implements Cloneable {

    private int ID;
    private String Nombre;
    private String Nombre1;
    private String Nombre2;
    private String Nombre3;
    private String Nombre4;
    private String Nombre5;
    private String Nombre6;
    private String Nombre7;
    private String Nombre8;
    private boolean Estado;
    private ArrayList detalle;

    public ModAuxiliar() {
        ID = 0;
        Nombre = "";
        Nombre1 = "";
        Nombre2 = "";
        Nombre3 = "";
        Nombre4 = "";
        Nombre5 = "";
        Nombre6 = "";
        Nombre7 = "";
        Nombre8 = "";
        Estado = true;
        detalle = new ArrayList();
    }

    public void addObjeto(ModAuxiliar mod) {
        this.detalle.add(mod);
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public ArrayList getDetalle() {
        return detalle;
    }

    public void setDetalle(ArrayList Detalle) {
        this.detalle = Detalle;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean Estado) {
        this.Estado = Estado;
    }

    public String getNombre1() {
        return Nombre1;
    }

    public void setNombre1(String Nombre1) {
        this.Nombre1 = Nombre1;
    }

    public String getNombre2() {
        return Nombre2;
    }

    public void setNombre2(String Nombre2) {
        this.Nombre2 = Nombre2;
    }

    public String getNombre3() {
        return Nombre3;
    }

    public void setNombre3(String Nombre3) {
        this.Nombre3 = Nombre3;
    }

    public String getNombre4() {
        return Nombre4;
    }

    public void setNombre4(String Nombre4) {
        this.Nombre4 = Nombre4;
    }

    public String getNombre5() {
        return Nombre5;
    }

    public void setNombre5(String Nombre5) {
        this.Nombre5 = Nombre5;
    }

    public String getNombre6() {
        return Nombre6;
    }

    public void setNombre6(String Nombre6) {
        this.Nombre6 = Nombre6;
    }

    public String getNombre7() {
        return Nombre7;
    }

    public void setNombre7(String Nombre7) {
        this.Nombre7 = Nombre7;
    }

    public String getNombre8() {
        return Nombre8;
    }

    public void setNombre8(String Nombre8) {
        this.Nombre8 = Nombre8;
    }

    public ModAuxiliar copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModAuxiliar) obj;
    }
}
