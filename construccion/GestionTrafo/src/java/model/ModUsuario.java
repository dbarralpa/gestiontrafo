package model;

import java.util.ArrayList;
import model.apl.ModEmpresa;

public class ModUsuario implements Cloneable {

    private int id;
    private int id_rol;
    private String nombre;
    private String apellido;
    private String user;
    private String pass;
    private String estado;
    private String trabajo;
    private ModRol Rol;
    private ModEmpresa Empresa;
    private ModPerfil Perfil;
    private ModPermiso Permiso;
    private ArrayList permisos;
    private ArrayList recursos;

    public ModUsuario() {
        id = 0;
        id_rol = 0;
        nombre = "";
        apellido = "";
        user = "";
        pass = "";
        estado = "";
        trabajo = "";
        Rol = null;
        Permiso = null;
        Perfil = null;
        Empresa = new ModEmpresa();
        permisos = new ArrayList();
        recursos = new ArrayList();
    }

    public ModPerfil getPerfil() {
        return Perfil;
    }

    public void setPerfil(ModPerfil Perfil) {
        this.Perfil = Perfil;
    }

    public ModPermiso getPermiso() {
        return Permiso;
    }

    public void setPermiso(ModPermiso Permiso) {
        this.Permiso = Permiso;
    }

    public ModRol getRol() {
        return Rol;
    }

    public void setRol(ModRol Rol) {
        this.Rol = Rol;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_rol() {
        return id_rol;
    }

    public void setId_rol(int id_rol) {
        this.id_rol = id_rol;
    }

    public ArrayList getPermisos() {
        return permisos;
    }

    public void setPermisos(ArrayList permisos) {
        this.permisos = permisos;
    }

    public ArrayList getRecursos() {
        return recursos;
    }

    public void setRecursos(ArrayList recursos) {
        this.recursos = recursos;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(String trabajo) {
        this.trabajo = trabajo;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public ModEmpresa getEmpresa() {
        return Empresa;
    }

    public void setEmpresa(ModEmpresa Empresa) {
        this.Empresa = Empresa;
    }
    
    public ModUsuario copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModUsuario) obj;
    }
}
