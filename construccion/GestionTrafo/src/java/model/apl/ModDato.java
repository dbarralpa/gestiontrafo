package model.apl;

import java.util.ArrayList;

/**
 *
 * @author dbarra
 */
public class ModDato implements Cloneable {

    private int id;
    private double vaMin;
    private double vaMax;
    private double promedio;
    private double desEstandar;
    private double resta;
    private double per5;
    private double per95;
    private ArrayList<ModDetalle> detalle;
    private ArrayList<ModDetalle> detInt;

    public ModDato() {
        id = 0;
        vaMin = 0;
        vaMax = 0;
        promedio = 0;
        desEstandar = 0;
        resta = 0;
        per5 = 0;
        per95 = 0;
        detalle = new ArrayList<ModDetalle>();
        detInt = new ArrayList<ModDetalle>();
    }

    public double getDesEstandar() {
        return desEstandar;
    }

    public void setDesEstandar(double desEstandar) {
        this.desEstandar = desEstandar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPer5() {
        return per5;
    }

    public void setPer5(double per5) {
        this.per5 = per5;
    }

    public double getPer95() {
        return per95;
    }

    public void setPer95(double per95) {
        this.per95 = per95;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }

    public double getResta() {
        return resta;
    }

    public void setResta(double resta) {
        this.resta = resta;
    }

    public double getVaMax() {
        return vaMax;
    }

    public void setVaMax(double vaMax) {
        this.vaMax = vaMax;
    }

    public double getVaMin() {
        return vaMin;
    }

    public void setVaMin(double vaMin) {
        this.vaMin = vaMin;
    }

    public ArrayList<ModDetalle> getDetalle() {
        return detalle;
    }

    public void setDetalle(ArrayList<ModDetalle> detalle) {
        this.detalle = detalle;
    }

    public ArrayList<ModDetalle> getDetInt() {
        return detInt;
    }

    public void setDetInt(ArrayList<ModDetalle> detInt) {
        this.detInt = detInt;
    }
    
    public ModDato copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModDato) obj;
    }
}
