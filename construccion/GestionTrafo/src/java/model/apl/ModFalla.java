/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.apl;

import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author dbarra
 */
public class ModFalla {

    private int id;
    private int idFalla;
    private int idFallaTrans;
    private byte idEstado;
    private String nombre;
    private boolean estado;
    private String color;
    private Timestamp lectura;
    private int dias;
    private String descripcion;
    String abrev;
    double max;
    ModVariable variable = new ModVariable();
    private ArrayList<ModBitacoraFalla> bitacora;
    private ArrayList<ModEstadistica> estadisticasFalla1;

    public ModFalla() {
        id = 0;
        idFalla = 0;
        idFallaTrans = 0;
        idEstado = 0;
        nombre = "";
        estado = false;
        color = "fallaNoVer";
        lectura = null;
        dias = 0;
        descripcion = "";
        abrev = "";
        max = 0;
        variable = new ModVariable();
        bitacora = new ArrayList<ModBitacoraFalla>();
        estadisticasFalla1 = new ArrayList<ModEstadistica>();
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdFalla() {
        return idFalla;
    }

    public void setIdFalla(int idFalla) {
        this.idFalla = idFalla;
    }

    public int getIdFallaTrans() {
        return idFallaTrans;
    }

    public void setIdFallaTrans(int idFallaTrans) {
        this.idFallaTrans = idFallaTrans;
    }

    public byte getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(byte idEstado) {
        this.idEstado = idEstado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

    public Timestamp getLectura() {
        return lectura;
    }

    public void setLectura(Timestamp lectura) {
        this.lectura = lectura;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAbrev() {
        return abrev;
    }

    public void setAbrev(String abrev) {
        this.abrev = abrev;
    }
    
    public ArrayList<ModBitacoraFalla> getBitacora() {
        return bitacora;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public ModVariable getVariable() {
        return variable;
    }

    public void setVariable(ModVariable variable) {
        this.variable = variable;
    }
    
    public void setBitacora(ArrayList<ModBitacoraFalla> bitacora) {
        this.bitacora = bitacora;
    }

    public ArrayList<ModEstadistica> getEstadisticasFalla1() {
        return estadisticasFalla1;
    }

    public void setEstadisticasFalla1(ArrayList<ModEstadistica> estadisticasFalla) {
        this.estadisticasFalla1 = estadisticasFalla;
    }
}
