/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.apl;

import java.sql.Timestamp;

/**
 *
 * @author dbarra
 */
public class ModParametro {

    private int id;
    private int idParam;
    private int idNom;
    private String nombre;
    private String descripcion;
    private String unidad;
    private String usuario;
    private String tipo;
    private String area;
    private double max;
    private double valor;
    private double maxTrans;
    private double maxEmpre;
    private double maxTrafonet;
    private boolean estadoTrans;
    private boolean estadoEmpre;
    private boolean estadoTrafonet;
    private Timestamp fecha;

    public ModParametro() {
        id = 0;
        nombre = "";
        unidad = "";
        usuario = "";
        area = "";
        descripcion = "";
        idNom = 0;
        max = 0d;
        valor = 0d;
        maxEmpre = 0d;
        maxTrafonet = 0d;
        maxTrans = 0d;
        idParam = 0;
        fecha = null;
        tipo = "";
    }

    public boolean isEstadoEmpre() {
        return estadoEmpre;
    }

    public void setEstadoEmpre(boolean estadoEmpre) {
        this.estadoEmpre = estadoEmpre;
    }

    public boolean isEstadoTrafonet() {
        return estadoTrafonet;
    }

    public void setEstadoTrafonet(boolean estadoTrafonet) {
        this.estadoTrafonet = estadoTrafonet;
    }

    public boolean isEstadoTrans() {
        return estadoTrans;
    }

    public void setEstadoTrans(boolean estadoTrans) {
        this.estadoTrans = estadoTrans;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdNom() {
        return idNom;
    }

    public void setIdNom(int idNom) {
        this.idNom = idNom;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }


    public double getMaxEmpre() {
        return maxEmpre;
    }

    public void setMaxEmpre(double maxEmpre) {
        this.maxEmpre = maxEmpre;
    }

    public double getMaxTrafonet() {
        return maxTrafonet;
    }

    public void setMaxTrafonet(double maxTrafonet) {
        this.maxTrafonet = maxTrafonet;
    }

    public double getMaxTrans() {
        return maxTrans;
    }

    public void setMaxTrans(double maxTrans) {
        this.maxTrans = maxTrans;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdParam() {
        return idParam;
    }

    public void setIdParam(int idParam) {
        this.idParam = idParam;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
