/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.apl;

import java.util.ArrayList;

/**
 *
 * @author dbarra
 */
public class ModTransformador {

    private int id;
    private int idTrans;
    private int posiTransfo;
    private String alias;
    private String modelo;
    private String descripcion;
    private String serie;
    private String fecha;
    private String status;
    private String fabricante;
    private String nomTabla;
    private ModFalla falla;
    private ArrayList<ModEstadistica> estadisticas;
    private ArrayList<ModEstadistica> setearFalla1;
    private ArrayList<ModFalla> fallas;
    private ArrayList detalles;
    private String red;

    public ModTransformador() {
        id = 0;
        idTrans = 0;
        posiTransfo = -1;
        alias = "";
        modelo = "";
        descripcion = "";
        serie = "";
        fecha = "";
        status = "";
        fabricante = "";
        nomTabla = "";
        red = "";
        falla = new ModFalla();
        estadisticas = new ArrayList<ModEstadistica>();
        fallas = new ArrayList<ModFalla>();
        detalles = new ArrayList();
        setearFalla1 = new ArrayList<ModEstadistica>();
    }
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTrans() {
        return idTrans;
    }

    public void setIdTrans(int idTrans) {
        this.idTrans = idTrans;
    }

    public int getPosiTransfo() {
        return posiTransfo;
    }

    public void setPosiTransfo(int posiTransfo) {
        this.posiTransfo = posiTransfo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getNomTabla() {
        return nomTabla;
    }

    public void setNomTabla(String nomTabla) {
        this.nomTabla = nomTabla;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModFalla getFalla() {
        return falla;
    }

    public void setFalla(ModFalla falla) {
        this.falla = falla;
    }

    public String getRed() {
        return red;
    }

    public void setRed(String red) {
        this.red = red;
    }

    public ArrayList<ModEstadistica> getEstadisticas() {
        return estadisticas;
    }

    public void setEstadisticas(ArrayList<ModEstadistica> estadisticas) {
        this.estadisticas = estadisticas;
    }

    public ArrayList<ModFalla> getFallas() {
        return fallas;
    }

    public void setFallas(ArrayList<ModFalla> fallas) {
        this.fallas = fallas;
    }

    public ArrayList getDetalles() {
        return detalles;
    }

    public void setDetalles(ArrayList detalles) {
        this.detalles = detalles;
    }

    public ArrayList<ModEstadistica> getSetearFalla1() {
        return setearFalla1;
    }

    public void setSetearFalla1(ArrayList<ModEstadistica> setearFalla1) {
        this.setearFalla1 = setearFalla1;
    }
}
