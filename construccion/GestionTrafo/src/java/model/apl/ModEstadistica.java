/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.apl;

import java.sql.Timestamp;

/**
 *
 * @author dbarra
 */
public class ModEstadistica implements Cloneable{

    private int id;
    private int idEsta;
    private double min;
    private double max;
    private double maxEmpre;
    private double maxTransfo;
    private double desv;
    private double prom;
    private double factorCarga;
    private double outBanda;
    private double inBanda;
    private double varCal;
    private double minLim;
    private double maxLim;
    private double tiempoCumplimiento;
    private boolean calDesv;
    private boolean calFacCar;
    private int cant;
    private int cantOutBand;
    private boolean analisis;
    private boolean analisEmpre;
    private boolean analisTranso;
    private boolean estado;
    private Timestamp ultimaActu;
    private String descriVari;

    public ModEstadistica() {
        min = 0;
        max = 0;
        maxEmpre = 0;
        maxTransfo = 0;
        desv = 0;
        prom = 0;
        factorCarga = 0;
        tiempoCumplimiento = 0;
        outBanda = 0;
        inBanda = 0;
        varCal = 0;
        minLim = 0;
        maxLim = 0;
        calDesv = false;
        calFacCar = false;
        cantOutBand = 0;
        analisis = false;
        analisEmpre = false;
        analisTranso = false;
        estado = false;
        ultimaActu = null;
        descriVari = "";
    }

    public double getTiempoCumplimiento() {
        return tiempoCumplimiento;
    }

    public void setTiempoCumplimiento(double tiempoCumplimiento) {
        this.tiempoCumplimiento = tiempoCumplimiento;
    }

    public double getDesv() {
        return desv;
    }

    public void setDesv(double desv) {
        this.desv = desv;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(int idEsta) {
        this.idEsta = idEsta;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public double getMaxEmpre() {
        return maxEmpre;
    }

    public void setMaxEmpre(double maxEmpre) {
        this.maxEmpre = maxEmpre;
    }

    public double getMaxTransfo() {
        return maxTransfo;
    }

    public void setMaxTransfo(double maxTransfo) {
        this.maxTransfo = maxTransfo;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getProm() {
        return prom;
    }

    public void setProm(double prom) {
        this.prom = prom;
    }

    public double getFactorCarga() {
        return factorCarga;
    }

    public void setFactorCarga(double factorCarga) {
        this.factorCarga = factorCarga;
    }

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    public double getInBanda() {
        return inBanda;
    }

    public void setInBanda(double inBanda) {
        this.inBanda = inBanda;
    }

    public double getOutBanda() {
        return outBanda;
    }

    public void setOutBanda(double outBanda) {
        this.outBanda = outBanda;
    }

    public boolean isAnalisis() {
        return analisis;
    }

    public boolean isAnalisEmpre() {
        return analisEmpre;
    }

    public void setAnalisEmpre(boolean analisEmpre) {
        this.analisEmpre = analisEmpre;
    }

    public boolean isAnalisTranso() {
        return analisTranso;
    }

    public void setAnalisTranso(boolean analisTranso) {
        this.analisTranso = analisTranso;
    }

    public void setAnalisis(boolean analisis) {
        this.analisis = analisis;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public double getVarCal() {
        return varCal;
    }

    public void setVarCal(double varCal) {
        this.varCal = varCal;
    }

    public double getMaxLim() {
        return maxLim;
    }

    public void setMaxLim(double maxLim) {
        this.maxLim = maxLim;
    }

    public double getMinLim() {
        return minLim;
    }

    public void setMinLim(double minLim) {
        this.minLim = minLim;
    }

    public boolean isCalDesv() {
        return calDesv;
    }

    public void setCalDesv(boolean calDesv) {
        this.calDesv = calDesv;
    }

    public boolean isCalFacCar() {
        return calFacCar;
    }

    public void setCalFacCar(boolean calFacCar) {
        this.calFacCar = calFacCar;
    }

    public int getCantOutBand() {
        return cantOutBand;
    }

    public void setCantOutBand(int cantOutBand) {
        this.cantOutBand = cantOutBand;
    }

    public Timestamp getUltimaActu() {
        return ultimaActu;
    }

    public void setUltimaActu(Timestamp ultimaActu) {
        this.ultimaActu = ultimaActu;
    }

    public String getDescriVari() {
        return descriVari;
    }

    public void setDescriVari(String descriVari) {
        this.descriVari = descriVari;
    }
    
    public ModEstadistica copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModEstadistica) obj;
    }
}
