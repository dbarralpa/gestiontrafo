package model.apl;

import java.util.ArrayList;

/**
 *
 * @author dbarra
 */
public class ModVariable implements Cloneable {

    private int id;
    private int id_desc;
    private String descripcion;
    private String abrevi;
    private boolean estado;
    private ModDato dato;
    private ModEstadistica estadistica;
    private ArrayList<ModEstadistica> estadisticas;

    public ModVariable() {
        estadisticas = new ArrayList<ModEstadistica>();
        id = 0;
        id_desc = 0;
        descripcion = "";
        abrevi = "";
        estado = false;
        dato = new ModDato();
        estadistica = new ModEstadistica();
    }

    public ModDato getDato() {
        return dato;
    }

    public void setDato(ModDato dato) {
        this.dato = dato;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAbrevi() {
        return abrevi;
    }

    public void setAbrevi(String abrevi) {
        this.abrevi = abrevi;
    }
    
    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_desc() {
        return id_desc;
    }

    public void setId_desc(int id_desc) {
        this.id_desc = id_desc;
    }

    public ArrayList<ModEstadistica> getEstadisticas() {
        return estadisticas;
    }

    public void setEstadisticas(ArrayList<ModEstadistica> estadisticas) {
        this.estadisticas = estadisticas;
    }

    public ModEstadistica getEstadistica() {
        return estadistica;
    }

    public void setEstadistica(ModEstadistica estadistica) {
        this.estadistica = estadistica;
    }

    public ModVariable copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModVariable) obj;
    }
}
