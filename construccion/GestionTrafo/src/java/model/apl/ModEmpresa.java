/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model.apl;

import java.util.ArrayList;

/**
 *
 * @author dbarra
 */
public class ModEmpresa {
 private int id;
 private int idEmpre;
 private int posiEmpre;
 private String nombre;
 private String schema;
 private ModTransformador transformador;
 private ModRed red;
 private ModParametro parametro;
 private ArrayList<ModTransformador> transformadores;
 private ArrayList<ModTransformador> transformadoresRed;
 private ArrayList<ModRed> redes;
 private ArrayList<ModFalla> allFallas;
 private ArrayList<ModParametro> parametros;
    public ModEmpresa() {
       id = 0;
       idEmpre = 0;
       posiEmpre = 0;
       nombre = "";
       schema = "";
       red = new ModRed();
       parametro = new ModParametro();
       transformador = new ModTransformador();
       transformadores = new ArrayList<ModTransformador>();
       transformadoresRed = new ArrayList<ModTransformador>();
       redes = new ArrayList<ModRed>();
       allFallas = new ArrayList<ModFalla>();
       parametros = new ArrayList<ModParametro>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEmpre() {
        return idEmpre;
    }

    public void setIdEmpre(int idEmpre) {
        this.idEmpre = idEmpre;
    }

    public int getPosiEmpre() {
        return posiEmpre;
    }

    public void setPosiEmpre(int posiEmpre) {
        this.posiEmpre = posiEmpre;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public ModTransformador getTransformador() {
        return transformador;
    }

    public void setTransformador(ModTransformador transformador) {
        this.transformador = transformador;
    }

    public ModRed getRed() {
        return red;
    }

    public void setRed(ModRed red) {
        this.red = red;
    }

    public ModParametro getParametro() {
        return parametro;
    }

    public void setParametro(ModParametro parametro) {
        this.parametro = parametro;
    }

    public ArrayList<ModTransformador> getTransformadores() {
        return transformadores;
    }

    public void setTransformadores(ArrayList<ModTransformador> transformadores) {
        this.transformadores = transformadores;
    }

    public ArrayList<ModTransformador> getTransformadoresRed() {
        return transformadoresRed;
    }

    public void setTransformadoresRed(ArrayList<ModTransformador> transformadoresRed) {
        this.transformadoresRed = transformadoresRed;
    }

    public ArrayList<ModParametro> getParametros() {
        return parametros;
    }

    public void setParametros(ArrayList<ModParametro> parametros) {
        this.parametros = parametros;
    }

    public ArrayList<ModFalla> getAllFallas() {
        return allFallas;
    }

    public void setAllFallas(ArrayList<ModFalla> allFallas) {
        this.allFallas = allFallas;
    }

    public ArrayList<ModRed> getRedes() {
        return redes;
    }

    public void setRedes(ArrayList<ModRed> redes) {
        this.redes = redes;
    }
}
