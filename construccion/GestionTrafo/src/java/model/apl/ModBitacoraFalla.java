/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.apl;

import java.sql.Timestamp;

/**
 *
 * @author dbarra
 */
public class ModBitacoraFalla {

    Timestamp fecha;
    String observacion;
    String usuario;

    public ModBitacoraFalla() {
        fecha = null;
        observacion = "";
        usuario = "";
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
