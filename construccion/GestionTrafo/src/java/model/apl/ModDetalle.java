package model.apl;

import java.sql.Timestamp;

/**
 *
 * @author dbarra
 */
public class ModDetalle implements Cloneable {

    private int id; 
    private Timestamp fecha;
    private double valor1;
    private double valor2;
    private double valor3;
    private double valor4;
    private double valor5;
    private double valor6;
    private double valor7;
    private double valor8;
    private double valor9;
    private double valor10;
    private double valor11;
    private double valor12;
    private double valor13;
    private double valor14;
    private double valor15;
    private double valor16;
    private double valor17;
    private double valor18;
    private double valor19;
    private double valor20;
    private double valor21;
    private double valor22;
    private double valor23;
    private double valor24;
    private double valor25;
    private double valor26;
    private double valor27;
    private double valor28;
    private double valor29;
    private double valor30;

    public ModDetalle() {
        id = 0;
        fecha = null;
        valor1 = 0d;
        valor2 = 0d;
        valor3 = 0d;
        valor4 = 0d;
        valor5 = 0d;
        valor6 = 0d;
        valor7 = 0d;
        valor8 = 0d;
        valor9 = 0d;
        valor10 = 0d;
        valor11 = 0d;
        valor12 = 0d;
        valor13 = 0d;
        valor14 = 0d;
        valor15 = 0d;
        valor16 = 0d;
        valor17 = 0d;
        valor18 = 0d;
        valor19 = 0d;
        valor20 = 0d;
        valor21 = 0d;
        valor22 = 0d;
        valor23 = 0d;
        valor24 = 0d;
        valor25 = 0d;
        valor26 = 0d;
        valor27 = 0d;
        valor28 = 0d;
        valor29 = 0d;
        valor30 = 0d;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValor1() {
        return valor1;
    }

    public void setValor1(double valor1) {
        this.valor1 = valor1;
    }

    public double getValor10() {
        return valor10;
    }

    public void setValor10(double valor10) {
        this.valor10 = valor10;
    }

    public double getValor11() {
        return valor11;
    }

    public void setValor11(double valor11) {
        this.valor11 = valor11;
    }

    public double getValor12() {
        return valor12;
    }

    public void setValor12(double valor12) {
        this.valor12 = valor12;
    }

    public double getValor13() {
        return valor13;
    }

    public void setValor13(double valor13) {
        this.valor13 = valor13;
    }

    public double getValor14() {
        return valor14;
    }

    public void setValor14(double valor14) {
        this.valor14 = valor14;
    }

    public double getValor15() {
        return valor15;
    }

    public void setValor15(double valor15) {
        this.valor15 = valor15;
    }

    public double getValor16() {
        return valor16;
    }

    public void setValor16(double valor16) {
        this.valor16 = valor16;
    }

    public double getValor17() {
        return valor17;
    }

    public void setValor17(double valor17) {
        this.valor17 = valor17;
    }

    public double getValor18() {
        return valor18;
    }

    public void setValor18(double valor18) {
        this.valor18 = valor18;
    }

    public double getValor19() {
        return valor19;
    }

    public void setValor19(double valor19) {
        this.valor19 = valor19;
    }

    public double getValor2() {
        return valor2;
    }

    public void setValor2(double valor2) {
        this.valor2 = valor2;
    }

    public double getValor20() {
        return valor20;
    }

    public void setValor20(double valor20) {
        this.valor20 = valor20;
    }

    public double getValor21() {
        return valor21;
    }

    public void setValor21(double valor21) {
        this.valor21 = valor21;
    }

    public double getValor22() {
        return valor22;
    }

    public void setValor22(double valor22) {
        this.valor22 = valor22;
    }

    public double getValor23() {
        return valor23;
    }

    public void setValor23(double valor23) {
        this.valor23 = valor23;
    }

    public double getValor24() {
        return valor24;
    }

    public void setValor24(double valor24) {
        this.valor24 = valor24;
    }

    public double getValor25() {
        return valor25;
    }

    public void setValor25(double valor25) {
        this.valor25 = valor25;
    }

    public double getValor26() {
        return valor26;
    }

    public void setValor26(double valor26) {
        this.valor26 = valor26;
    }

    public double getValor27() {
        return valor27;
    }

    public void setValor27(double valor27) {
        this.valor27 = valor27;
    }

    public double getValor28() {
        return valor28;
    }

    public void setValor28(double valor28) {
        this.valor28 = valor28;
    }

    public double getValor29() {
        return valor29;
    }

    public void setValor29(double valor29) {
        this.valor29 = valor29;
    }

    public double getValor3() {
        return valor3;
    }

    public void setValor3(double valor3) {
        this.valor3 = valor3;
    }

    public double getValor30() {
        return valor30;
    }

    public void setValor30(double valor30) {
        this.valor30 = valor30;
    }

    public double getValor4() {
        return valor4;
    }

    public void setValor4(double valor4) {
        this.valor4 = valor4;
    }

    public double getValor5() {
        return valor5;
    }

    public void setValor5(double valor5) {
        this.valor5 = valor5;
    }

    public double getValor6() {
        return valor6;
    }

    public void setValor6(double valor6) {
        this.valor6 = valor6;
    }

    public double getValor7() {
        return valor7;
    }

    public void setValor7(double valor7) {
        this.valor7 = valor7;
    }

    public double getValor8() {
        return valor8;
    }

    public void setValor8(double valor8) {
        this.valor8 = valor8;
    }

    public double getValor9() {
        return valor9;
    }

    public void setValor9(double valor9) {
        this.valor9 = valor9;
    }

    public ModDetalle copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModDetalle) obj;
    }
}
