package model;

public class ModPermiso implements Cloneable {

    private int id_tipo_permiso;
    private int id_recurso_padre;
    private int id_recurso_hijo;
    private int id_perfil;
    private String vc_usuario;
    private String f_asig;
    private ModTipoPermiso TipoPermiso;
    private ModPerfil Perfil;
    private ModRecurso Recurso;

    public ModPermiso() {
        id_tipo_permiso = 0;
        id_recurso_padre = 0;
        id_recurso_hijo = 0;
        id_perfil = 0;
        vc_usuario = "";
        f_asig = "";
        TipoPermiso = null;
        Perfil = null;
    }

    public ModPerfil getPerfil() {
        return Perfil;
    }

    public void setPerfil(ModPerfil Perfil) {
        this.Perfil = Perfil;
    }

    public ModRecurso getRecurso() {
        return Recurso;
    }

    public void setRecurso(ModRecurso Recurso) {
        this.Recurso = Recurso;
    }

    public ModTipoPermiso getTipoPermiso() {
        return TipoPermiso;
    }

    public void setTipoPermiso(ModTipoPermiso TipoPermiso) {
        this.TipoPermiso = TipoPermiso;
    }

    public String getF_asig() {
        return f_asig;
    }

    public void setF_asig(String f_asig) {
        this.f_asig = f_asig;
    }

    public int getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }

    public int getId_recurso_hijo() {
        return id_recurso_hijo;
    }

    public void setId_recurso_hijo(int id_recurso_hijo) {
        this.id_recurso_hijo = id_recurso_hijo;
    }

    public int getId_recurso_padre() {
        return id_recurso_padre;
    }

    public void setId_recurso_padre(int id_recurso_padre) {
        this.id_recurso_padre = id_recurso_padre;
    }

    public int getId_tipo_permiso() {
        return id_tipo_permiso;
    }

    public void setId_tipo_permiso(int id_tipo_permiso) {
        this.id_tipo_permiso = id_tipo_permiso;
    }

    public String getVc_usuario() {
        return vc_usuario;
    }

    public void setVc_usuario(String vc_usuario) {
        this.vc_usuario = vc_usuario;
    }

    public ModPermiso copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModPermiso) obj;
    }
}
