package model;

public class ModTipoPermiso implements Cloneable {

    private int id_t_perm;    
    private String nombre_tipo_permiso;
    private String desc_tipo_permiso;
    private String estado_tipo_permiso;

    public ModTipoPermiso() {
        id_t_perm = 0;        
        nombre_tipo_permiso = "";
        desc_tipo_permiso = "";
        estado_tipo_permiso = "";
    }

    public int getId_t_perm() {
        return id_t_perm;
    }

    public void setId_t_perm(int id_t_perm) {
        this.id_t_perm = id_t_perm;
    }    

    public String getDesc_tipo_permiso() {
        return desc_tipo_permiso;
    }

    public void setDesc_tipo_permiso(String desc_tipo_permiso) {
        this.desc_tipo_permiso = desc_tipo_permiso;
    }

    public String getEstado_tipo_permiso() {
        return estado_tipo_permiso;
    }

    public void setEstado_tipo_permiso(String estado_tipo_permiso) {
        this.estado_tipo_permiso = estado_tipo_permiso;
    }

    public String getNombre_tipo_permiso() {
        return nombre_tipo_permiso;
    }

    public void setNombre_tipo_permiso(String nombre_tipo_permiso) {
        this.nombre_tipo_permiso = nombre_tipo_permiso;
    }

    public ModTipoPermiso copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModTipoPermiso) obj;
    }
}
