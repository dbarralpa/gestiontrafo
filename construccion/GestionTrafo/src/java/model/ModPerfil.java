package model;

public class ModPerfil implements Cloneable {

    private int id;    
    private int id_perfil;
    private String nombre_perfil;
    private String desc_perfil;
    private String estado_perfil;

    public ModPerfil() {
        id = 0;        
        id_perfil = 0;
        nombre_perfil = "";
        desc_perfil = "";
        estado_perfil = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_perfil() {
        return id_perfil;
    }

    public void setId_perfil(int id_perfil) {
        this.id_perfil = id_perfil;
    }
    
    public String getNombre_perfil() {
        return nombre_perfil;
    }

    public void setNombre_perfil(String nombre_perfil) {
        this.nombre_perfil = nombre_perfil;
    }

    public String getDesc_perfil() {
        return desc_perfil;
    }

    public void setDesc_perfil(String desc_perfil) {
        this.desc_perfil = desc_perfil;
    }

    public String getEstado_perfil() {
        return estado_perfil;
    }

    public void setEstado_perfil(String estado_perfil) {
        this.estado_perfil = estado_perfil;
    }

    public ModPerfil copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModPerfil) obj;
    }
}
