package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import model.ModSesion;
import model.ModUsuario;
import model.apl.ModBitacoraFalla;
import model.apl.ModEmpresa;
import model.apl.ModEstadistica;
import model.apl.ModFalla;
import model.apl.ModParametro;
import model.apl.ModRed;
import model.apl.ModTransformador;
import model.apl.ModVariable;
import sql.ConexionSQL;

public class PrmDato {

    public PrmDato() {
    }

    public static String autenticaUsuario(String user, String pass) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        String Aux = "ERROR";
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                rs = cn.SelectReg("trafonet_main.tbl_usuario", "vc_estado", "vc_usuario = '" + user + "' AND vc_clave = '" + pass + "'");

                if (rs.next()) {
                    Aux = rs.getString("vc_estado");
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return Aux;
    }

    public static ModUsuario cargarUsuario(String user, String pass) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ModUsuario usuario = new ModUsuario();
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                rs = cn.SelectReg("trafonet_main.tbl_usuario", "vc_usuario = '" + user + "' AND vc_clave = '" + pass + "'");
                if (rs.next()) {
                    usuario.setId_rol(1);
                    usuario.setNombre(PrmApp.verHtmlNull(rs.getString("vc_nombre")));
                    usuario.setApellido(PrmApp.verHtmlNull(rs.getString("vc_apellido")));
                    usuario.setUser(PrmApp.verHtmlNull(rs.getString("vc_usuario")));
                    usuario.setPass(PrmApp.verHtmlNull(rs.getString("vc_clave")));
                    usuario.setEstado(PrmApp.verHtmlNull(rs.getString("vc_estado")));
                    usuario.setEmpresa(cargarEmpresa(rs.getInt("empresa_id_emp")));
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return usuario;
    }

    public static ModEmpresa cargarEmpresa(int empre) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ModEmpresa empresa = null;

        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                rs = cn.SelectReg("trafonet_main.tbl_empresa", "vc_nombre,vc_schema", "id_emp =" + empre);
                while (rs.next()) {
                    empresa = new ModEmpresa();
                    empresa.setIdEmpre(empre);
                    empresa.setNombre(rs.getString("vc_nombre"));
                    empresa.setSchema("trafonet_data_" + rs.getString("vc_schema"));
                    empresa.setTransformadores(cargarTransformadores(empre));
                    empresa.setRedes(cargarRedes(empre));
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return empresa;
    }

    public static ArrayList<ModRed> cargarRedes(int empre) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ModRed red = null;
        ArrayList<ModRed> redes = null;
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                redes = new ArrayList<ModRed>();
                rs = cn.SelectReg("trafonet_main.tbl_red_empresa", "nb_id_red,vc_nombre", " nb_id_empresa=" + empre);
                while (rs.next()) {
                    red = new ModRed();
                    red.setId(redes.size());
                    red.setIdRed(rs.getInt("nb_id_red"));
                    red.setNombre(rs.getString("vc_nombre"));
                    redes.add(red);
                    red = null;
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return redes;
    }

    public static ArrayList cargarDatosGrafico(String schema, String tabla, Timestamp fecha, int dias) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList datos = new ArrayList();

        try {
            long ax = dias * 86400000;
            Timestamp time = new Timestamp(fecha.getTime() - ax);
            //int rango = rango(schema, tabla, time, fecha);
            int rango = 1;
            int incre = 1;
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                rs = cn.SelectReg(schema + "." + tabla, " dt_ultlectura,val1, val2, val3, val4, val5, val6, val7, val8, val9, "
                        + " val10, val11, val12, val13, val14, val15, val16, val17, val18, "
                        + " val19, val20, val21, val22, val23, val24, val25, val26, val27, "
                        //+ " val28, val29, val30", "dt_ultlectura >= '" + time + "' and dt_ultlectura <= '" + fecha + "'",
                        + " val28, val29, val30", "dt_ultlectura >= '2011-08-01 00:00:00' and dt_ultlectura <= '2011-08-01 08:15:00'",
                        "dt_ultlectura");
                while (rs.next()) {
                    if (incre == rango) {
                        ArrayList detalle = new ArrayList();
                        detalle.add(rs.getTimestamp("dt_ultlectura"));
                        if (rs.getDouble("val1") != -1.1) {
                            detalle.add(rs.getDouble("val1"));
                            detalle.add(rs.getDouble("val2"));
                            detalle.add(rs.getDouble("val3"));
                            detalle.add(rs.getDouble("val4"));
                            detalle.add(rs.getDouble("val5"));
                            detalle.add(rs.getDouble("val6"));
                            detalle.add(rs.getDouble("val7"));
                            detalle.add(rs.getDouble("val8"));
                            detalle.add(rs.getDouble("val9"));
                            detalle.add(rs.getDouble("val10"));
                            detalle.add(rs.getDouble("val11"));
                            detalle.add(rs.getDouble("val12"));
                            detalle.add(rs.getDouble("val13"));
                            detalle.add(rs.getDouble("val14"));
                            detalle.add(rs.getDouble("val15"));
                            detalle.add(rs.getDouble("val16"));
                            detalle.add(rs.getDouble("val17"));
                            detalle.add(rs.getDouble("val18"));
                            detalle.add(rs.getDouble("val19"));
                            detalle.add(rs.getDouble("val20"));
                            detalle.add(rs.getDouble("val21"));
                            detalle.add(rs.getDouble("val22"));
                            detalle.add(rs.getDouble("val23"));
                            detalle.add(rs.getDouble("val24"));
                            detalle.add(rs.getDouble("val25"));
                            detalle.add(rs.getDouble("val26"));
                            detalle.add(rs.getDouble("val27"));
                            detalle.add(rs.getDouble("val28"));
                            detalle.add(rs.getDouble("val29"));
                            detalle.add(rs.getDouble("val30"));
                        } else {
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);
                            detalle.add(null);

                        }
                        datos.add(detalle);
                        incre = 0;
                    }
                    incre++;
                }

                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return datos;
    }

    public static ArrayList<ModTransformador> cargarTransformadores(int empre) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList transformadores = new ArrayList();
        String sql = null;
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                /*rs = cn.SelectReg("trafonet_main.tbl_transformador", "vc_alias, vc_modelo, vc_descripcion, vc_serie, vc_fecha, "
                        + " vc_status,vc_fabricante, id_trans_bd", "empresa_id_emp = " + empre + " AND dt_fecha_lectura is not null", "id_trans_bd");*/
                sql  = " SELECT vc_alias, vc_modelo, vc_descripcion, vc_serie, vc_fecha,  vc_status,vc_fabricante, id_trans_bd,re.vc_nombre " ;
                sql += " FROM trafonet_main.tbl_transformador ";
                sql += " INNER JOIN trafonet_main.tbl_red_trans rt ";
                sql += " ON(id_trans_bd=nb_id_trans) ";
                sql += " INNER JOIN trafonet_main.tbl_red_empresa re ";
                sql += " ON(re.nb_id_red=rt.nb_id_red_empre) ";
                sql += " WHERE empresa_id_emp = 1 AND dt_fecha_lectura is not null ";
                sql += " ORDER BY id_trans_bd ";
                rs = cn.ExecuteQuery(sql);

                while (rs.next()) {
                    ModTransformador trans = new ModTransformador();
                    trans.setId(transformadores.size());
                    trans.setIdTrans(rs.getInt("id_trans_bd"));
                    trans.setAlias(rs.getString("vc_alias"));
                    trans.setModelo(rs.getString("vc_modelo"));
                    trans.setDescripcion(rs.getString("vc_descripcion"));
                    trans.setSerie(rs.getString("vc_serie"));
                    trans.setFecha(rs.getString("vc_fecha"));
                    trans.setStatus(rs.getString("vc_status"));
                    trans.setFabricante(rs.getString("vc_fabricante"));
                    trans.setRed(rs.getString("vc_nombre"));
                    trans.setNomTabla("tbl_medicion_" + trans.getIdTrans());
                    trans.setEstadisticas(cargarEstadisticasTransformadores(trans.getIdTrans(), empre));
                    transformadores.add(trans);
                    trans = null;
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return transformadores;
    }

    public static ArrayList<ModEstadistica> cargarEstadisticasTransformadores(int trans, int empre) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList estadisticas = null;
        String sql = "";
        byte count = 0;
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql = " SELECT nb_min, nb_max, nb_desv, nb_promedio, nb_id_var, nb_ultimafecha, nb_factorcarga, ";
                sql += " nb_por_out_band, nb_cant_out_band ";
                sql += " FROM trafonet_main.tbl_estadisticas  ";
                sql += " WHERE nb_id_trans = " + trans + " AND sm_leer = -1 AND nb_min > -1 ";
                sql += " ORDER BY nb_id_var ";
                rs = cn.ExecuteQuery(sql);
                estadisticas = new ArrayList();
                ModParametro param = null;
                while (rs.next()) {
                    ModEstadistica estadi = new ModEstadistica();
                    estadi.setId(estadisticas.size());
                    estadi.setMin(rs.getDouble("nb_min"));
                    estadi.setMax(rs.getDouble("nb_max"));
                    estadi.setProm(rs.getDouble("nb_promedio"));
                    estadi.setIdEsta(rs.getInt("nb_id_var"));
                    estadi.setUltimaActu(rs.getTimestamp("nb_ultimafecha"));
                    estadi.setFactorCarga(rs.getDouble("nb_factorcarga"));
                    estadi.setOutBanda(rs.getDouble("nb_por_out_band"));
                    estadi.setCantOutBand(rs.getInt("nb_cant_out_band"));
                    //param = cargarParametrosEstadisticas(trans, empre, estadi.getIdEsta(), "min");
                    estadi.setMinLim(80);
                    //param = null;
                    //param = cargarParametrosEstadisticas(trans, empre, estadi.getIdEsta(), "max");
                    estadi.setMaxLim(200);
                    estadisticas.add(estadi);
                    //estadi = null;
                    count++;
                }
                rs.close();
                rs = null;
                if(count == 0){
                Timestamp fecha = null;
                sql  = " SELECT max(dt_fecha_fin)";
                sql += " FROM trafonet_main.tbl_estadisticas ";
                sql += " WHERE nb_id_trans =  " + trans + " AND  nb_min > -1 ";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    fecha = rs.getTimestamp(1);
                }
                rs.close();
                rs = null;
                
                sql = " SELECT nb_min, nb_max, nb_desv, nb_promedio, nb_id_var, nb_ultimafecha, nb_factorcarga, ";
                sql += " nb_por_out_band, nb_cant_out_band ";
                sql += " FROM trafonet_main.tbl_estadisticas  ";
                sql += " WHERE nb_id_trans = " + trans + " AND dt_fecha_fin = '"+fecha+"' AND nb_min > -1 ";
                sql += " ORDER BY nb_id_var ";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModEstadistica estadi = new ModEstadistica();
                    estadi.setId(estadisticas.size());
                    estadi.setMin(rs.getDouble("nb_min"));
                    estadi.setMax(rs.getDouble("nb_max"));
                    estadi.setProm(rs.getDouble("nb_promedio"));
                    estadi.setIdEsta(rs.getInt("nb_id_var"));
                    estadi.setUltimaActu(rs.getTimestamp("nb_ultimafecha"));
                    estadi.setFactorCarga(rs.getDouble("nb_factorcarga"));
                    estadi.setOutBanda(rs.getDouble("nb_por_out_band"));
                    estadi.setCantOutBand(rs.getInt("nb_cant_out_band"));
                    //param = cargarParametrosEstadisticas(trans, empre, estadi.getIdEsta(), "min");
                    estadi.setMinLim(80);
                    //param = null;
                    //param = cargarParametrosEstadisticas(trans, empre, estadi.getIdEsta(), "max");
                    estadi.setMaxLim(200);
                    estadisticas.add(estadi);
                    //estadi = null;
                }
                rs.close();
                rs = null;
                }
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return estadisticas;
    }

    private static ModParametro cargarParametrosEstadisticas(int trans, int empre, int idParam, String nombre) {
        ConexionSQL cn = new ConexionSQL();
        ResultSet rs = null;
        String sql = null;
        String val = "";
        int id = -1;
        ModParametro param = null;
        try {
            /*if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql = " SELECT nb_id_param  FROM trafonet_main.tbl_parametros_trafonet ";
                sql += " WHERE vc_nombre = '" + nombre + "' AND nb_id_nom=" + idParam;
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    id = rs.getInt("nb_id_param");
                }
                rs.close();
                rs = null;

                sql = " SELECT nb_id_param_empre FROM trafonet_main.tbl_parametros ";
                sql += " WHERE nb_id_parametros_trafonet =" + id;
                sql += " AND ((nb_id_trans = " + trans + " AND nb_id_empre = " + empre + ") OR nb_id_trans = -1 AND nb_id_empre = " + empre + " OR nb_id_trans = -1 AND nb_id_empre = -1) ";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    val += rs.getInt("nb_id_param_empre") + ",";
                }
                rs.close();
                rs = null;

                sql = " SELECT nb_valor ";
                sql += " FROM trafonet_main.tbl_bitacora_parametros ";
                sql += " INNER JOIN trafonet_main.tbl_parametros ";
                sql += " ON(nb_id_param_empre = nb_id_parametros) ";
                sql += " WHERE nb_id_parametros IN(" + val.substring(0, val.length() - 1) + ") ";
                sql += " AND dt_fecha = (SELECT MAX(dt_fecha) ";
                sql += " FROM trafonet_main.tbl_bitacora_parametros WHERE nb_id_parametros IN(" + val.substring(0, val.length() - 1) + ")) ";

                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    param = new ModParametro();
                    param.setValor(rs.getDouble("nb_valor"));
                }
                rs.close();
                rs = null;
            }*/
        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
        return param;
    }

//    private static double cargarLimitesVariablesTransEmpre(int id, int trans, int empre, String campo) {
//        ResultSet rs = null;
//        ConexionSQL cn = new ConexionSQL();
//        String sql = "";
//        double valor = 0d;
//
//        try {
//            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
//                sql += " SELECT " + campo + " as VALOR FROM trafonet_main.tbl_limites_variable_empresa ";
//                sql += " WHERE nb_id_empresa = " + empre + " AND nb_id_trans = " + trans + " AND nb_id_var_lim = " + id;
//                rs = cn.ExecuteQuery(sql);
//
//                while (rs.next()) {
//                    valor = rs.getDouble("VALOR");
//                }
//                rs.close();
//                rs = null;
//                if (valor == 0) {
//                    sql = "";
//                    sql += " SELECT " + campo + " as VALOR FROM trafonet_main.tbl_limites_variable_empresa ";
//                    sql += " WHERE nb_id_empresa = " + empre + " AND nb_id_trans = -1 AND nb_id_var_lim = " + id;
//                    rs = cn.ExecuteQuery(sql);
//
//                    while (rs.next()) {
//                        valor = rs.getDouble("VALOR");
//                    }
//                    rs.close();
//                    rs = null;
//                }
//                if (valor == 0) {
//                    sql = "";
//                    sql += " SELECT " + campo + " as VALOR FROM trafonet_main.tbl_limites_variable_empresa ";
//                    sql += " WHERE nb_id_empresa = -1 AND nb_id_trans = -1 AND nb_id_var_lim =" + id;
//                    rs = cn.ExecuteQuery(sql);
//
//                    while (rs.next()) {
//                        valor = rs.getDouble("VALOR");
//                    }
//                    rs.close();
//                    rs = null;
//                }
//            }
//            cn.closeSQL();
//        } catch (SQLException ex) {
//            PrmApp.addError(ex, true);
//        }
//        return valor;
//    }
    public static ArrayList<ModEstadistica> cargarEstadisticasTransformadoresPorFalla1(Timestamp fechaIni, Timestamp fechaFin) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList estadisticas = null;
        String sql = "";
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                //rs = cn.SelectReg(schema + ".estadisticas", "nb_min, nb_max, nb_desv, nb_promedio, nb_id_var, nb_ultimafecha, nb_factorcarga, nb_por_out_band, nb_cant_out_band", "nb_id_trans = " + id + " AND sm_leer = -1","nb_id_var");
                sql = " SELECT nb_min, nb_max, nb_desv, nb_promedio, nb_id_var, nb_factorcarga, ";
                sql += " nb_por_out_band, nb_cant_out_band";
                sql += " FROM trafonet_main.tbl_estadisticasfallas ";
                //sql += " WHERE nb_id_falla_trans = " + id;
                sql += " WHERE dt_fecha_inicio = '"+fechaIni+"' and dt_fecha_fin = '"+fechaFin+"'";
                sql += " ORDER BY nb_id_var  ";
                rs = cn.ExecuteQuery(sql);
                estadisticas = new ArrayList();
                while (rs.next()) {
                    ModEstadistica estadi = new ModEstadistica();
                    estadi.setId(estadisticas.size());
                    estadi.setMin(rs.getDouble("nb_min"));
                    estadi.setMax(rs.getDouble("nb_max"));
                    estadi.setProm(rs.getDouble("nb_promedio"));
                    estadi.setIdEsta(rs.getInt("nb_id_var"));
                    estadi.setFactorCarga(rs.getDouble("nb_factorcarga"));
                    estadi.setOutBanda(rs.getDouble("nb_por_out_band"));
                    estadi.setCantOutBand(rs.getInt("nb_cant_out_band"));
                    estadisticas.add(estadi);
                    estadi = null;
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return estadisticas;
    }

    public static ArrayList<ModFalla> cargarFallas(int idTrans) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList fallas = new ArrayList();
        String sql = "";
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql += " SELECT fatra.nb_id_falla AS nb_id_falla,nb_id_falla_trans,vc_descri_falla,vc_nombre,fatra.bo_estado as bo_estado,vc_class,fatra.dt_fecha as dt_fecha,nb_tiempo_atras,nb_valor,vc_abrevi,dt_fecha_ini,dt_fecha_fin ";
                sql += " FROM trafonet_main.tbl_fallas fa ";
                sql += " INNER JOIN trafonet_main.tbl_fallas_trans fatra ";
                sql += " ON(fa.nb_id_falla = fatra.nb_id_falla) ";
                sql += " WHERE nb_id_trans = " + idTrans + " AND fatra.bo_estado = true ";
                sql += " ORDER BY fa.nb_id_falla";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModFalla falla = new ModFalla();
                    falla.setId(fallas.size());
                    falla.setIdFalla(rs.getInt("nb_id_falla"));
                    falla.setIdFallaTrans(rs.getInt("nb_id_falla_trans"));
                    falla.setDescripcion(rs.getString("vc_descri_falla"));
                    falla.setNombre(rs.getString("vc_nombre"));
                    falla.setEstado(rs.getBoolean("bo_estado"));
                    falla.setColor(rs.getString("vc_class"));
                    falla.setLectura(rs.getTimestamp("dt_fecha"));
                    falla.setDias(rs.getInt("nb_tiempo_atras"));
                    falla.setAbrev(rs.getString("vc_abrevi"));
                    falla.setMax(rs.getDouble("nb_valor"));
                    falla.setEstadisticasFalla1(cargarEstadisticasTransformadoresPorFalla1(rs.getTimestamp("dt_fecha_ini"),rs.getTimestamp("dt_fecha_fin")));
                    fallas.add(falla);
                    falla = null;
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return fallas;
    }

    public static ArrayList cargarVariables() {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList variables = null;
        String sql = null;

        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql = " SELECT id_desc,vc_variable,vc_abreviatura FROM trafonet_main.tbl_descripcion_variable ";
                sql += " WHERE vc_estado = 'VIGENT' ";
                sql += " ORDER BY id_desc  ";
                rs = cn.ExecuteQuery(sql);
                variables = new ArrayList();
                while (rs.next()) {
                    ModVariable variable = new ModVariable();
                    variable.setId(variables.size());
                    variable.setId_desc(rs.getInt("id_desc"));
                    variable.setDescripcion(rs.getString("vc_variable"));
                    variable.setAbrevi(rs.getString("vc_abreviatura"));
                    variables.add(variable);
                    variable = null;
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return variables;
    }

    private static int rango(String schema, String tabla, Timestamp fecha, Timestamp fecha1) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        String sql = null;
        int val = 0;
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql = " select  count(dt_ultlectura) AS delta";
                sql += " from " + schema + "." + tabla + " ";
                sql += " where dt_ultlectura >= '" + fecha + "' and dt_ultlectura <= '" + fecha1 + "' ";

                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    val = rs.getInt("delta");
                }
                if (val > 1000) {
                    val = val / 1000;
                } else {
                    val = 1;
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return val;
    }

    public static ArrayList<ModBitacoraFalla> cargarBitacoraFallas(int idFalla) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        String sql = null;
        ArrayList fallas = new ArrayList();

        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql = " SELECT  dt_fecha, vc_observacion, vc_usuario";
                sql += " FROM trafonet_main.tbl_bitacora_fallas";
                sql += " WHERE nb_id_fallas_trans = '" + idFalla + "' ORDER BY nb_id_fallas_trans";

                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    ModBitacoraFalla bita = new ModBitacoraFalla();
                    bita.setFecha(rs.getTimestamp("dt_fecha"));
                    bita.setObservacion(rs.getString("vc_observacion"));
                    bita.setUsuario(rs.getString("vc_usuario"));
                    fallas.add(bita);
                    bita = null;
                }

                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return fallas;
    }

    public static boolean cargarLimitesParametros(int trans, int empre, ModParametro param) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        String sql = "";
        int val = 0;
        boolean bo = true;
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql = " select nb_valor ";
                sql += " from trafonet_main.tbl_parametros ";
                sql += " where nb_id_parametros_trafonet = " + param.getIdParam() + " and ";
                sql += " nb_id_trans = " + trans + " and nb_id_empre = " + empre + " ";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    param.setMaxTrans(rs.getDouble("nb_valor"));
                    param.setEstadoTrans(true);
                    val++;
                }
                rs.close();
                rs = null;
                //if (val == 0) {
                sql = " select nb_valor ";
                sql += " from trafonet_main.tbl_parametros ";
                sql += " where nb_id_parametros_trafonet = " + param.getIdParam() + " and ";
                sql += " nb_id_trans = -1 and nb_id_empre = " + empre + " ";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    param.setMaxEmpre(rs.getDouble("nb_valor"));
                    param.setEstadoEmpre(true);
                    val++;
                }
                rs.close();
                rs = null;
                //   }

                // if (val == 0) {
                sql = " select nb_valor ";
                sql += " from trafonet_main.tbl_parametros ";
                sql += " where nb_id_parametros_trafonet = " + param.getIdParam() + " and ";
                sql += " nb_id_trans = -1 and nb_id_empre = -1 ";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    param.setMaxTrafonet(rs.getDouble("nb_valor"));
                    param.setEstadoTrafonet(true);
                }
                rs.close();
                rs = null;
            }
            // }

            cn.closeSQL();
        } catch (SQLException ex) {
            bo = false;
            PrmApp.addError(ex, true);
        }
        return bo;
    }

    public static ArrayList<ModFalla> cargarAllFallas() {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList<ModFalla> fallas = null;
        String sql = null;

        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql = " SELECT nb_id_falla,vc_nombre,vc_descri_falla FROM trafonet_main.tbl_fallas";
                rs = cn.ExecuteQuery(sql);
                fallas = new ArrayList();
                while (rs.next()) {
                    ModFalla falla = new ModFalla();
                    falla.setId(fallas.size());
                    falla.setIdFalla(rs.getInt("nb_id_falla"));
                    falla.setNombre(rs.getString("vc_nombre"));
                    falla.setDescripcion(rs.getString("vc_descri_falla"));
                    fallas.add(falla);
                    falla = null;
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return fallas;
    }

    public static ArrayList<ModParametro> cargarParametros(int idFalla, int trans, int empre, String tipo) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList<ModParametro> parametros = null;
        String sql = null;

        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql = " SELECT nb_id_param,vc_descripcion,tra.vc_nombre,tra.nb_id_nom,tra.vc_tipo,vc_unidad ";
                sql += " FROM trafonet_main.tbl_parametros_trafonet tra";
                if (idFalla > 0) {
                    sql += " INNER JOIN trafonet_main.tbl_nub_fallas_parametros ";
                    sql += " ON(nb_id_param = nb_id_parametros) ";
                    sql += " WHERE nb_id_falla = " + idFalla + " ";

                }
                if (tipo != null && !tipo.equals("Z")) {
                    sql += " WHERE vc_tipo = '" + tipo + "' ";
                }
                sql += " ORDER BY nb_id_param ";

                rs = cn.ExecuteQuery(sql);
                parametros = new ArrayList();
                while (rs.next()) {
                    ModParametro parametro = new ModParametro();
                    parametro.setId(parametros.size());
                    parametro.setIdParam(rs.getInt("nb_id_param"));
                    parametro.setDescripcion(rs.getString("vc_descripcion"));
                    parametro.setNombre(rs.getString("vc_nombre"));
                    parametro.setIdNom(rs.getInt("nb_id_nom"));
                    ModParametro param = cargarParametrosAnexos(trans, empre, rs.getInt("nb_id_param"), rs.getString("vc_nombre"));
                    parametro.setFecha(param.getFecha());
                    parametro.setUsuario(param.getUsuario());
                    parametro.setArea(param.getArea());
                    parametro.setMax(param.getMax());
                    parametro.setTipo(PrmApp.devolverTipo(rs.getString("vc_tipo")));
                    parametro.setUnidad(rs.getString("vc_unidad"));
                    parametros.add(parametro);
                    parametro = null;
                    param = null;
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return parametros;
    }

    public static ModParametro cargarParametrosAnexos(int trans, int empre, int idParam, String nombre) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        String sql = null;
        String val = "";
        ModParametro param = null;
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql = " SELECT nb_id_param_empre FROM trafonet_main.tbl_parametros ";
                sql += " WHERE nb_id_parametros_trafonet =" + idParam;
                sql += " AND ((nb_id_trans = " + trans + " AND nb_id_empre = " + empre + ") OR nb_id_trans = -1 AND nb_id_empre = " + empre + " OR nb_id_trans = -1 AND nb_id_empre = -1) ";
                //sql += " AND vc_nombre = '" + nombre + "'";
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    val += rs.getInt("nb_id_param_empre") + ",";
                }
                rs.close();
                rs = null;

                sql = " SELECT dt_fecha,vc_usuario,vc_area,nb_valor ";
                sql += " FROM trafonet_main.tbl_bitacora_parametros ";
                sql += " INNER JOIN trafonet_main.tbl_parametros ";
                sql += " ON(nb_id_param_empre = nb_id_parametros) ";
                sql += " WHERE nb_id_parametros IN(" + val.substring(0, val.length() - 1) + ") ";
                sql += " AND dt_fecha = (SELECT MAX(dt_fecha) ";
                sql += " FROM trafonet_main.tbl_bitacora_parametros WHERE nb_id_parametros IN(" + val.substring(0, val.length() - 1) + ")) ";

                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    param = new ModParametro();
                    param.setFecha(rs.getTimestamp("dt_fecha"));
                    param.setUsuario(rs.getString("vc_usuario"));
                    param.setArea(rs.getString("vc_area"));
                    param.setMax(rs.getDouble("nb_valor"));
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return param;
    }

    public static ArrayList<ModTransformador> cargarTransformadoresRed(int idRed) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList<ModTransformador> trans = null;
        String sql = null;

        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql = " SELECT nb_id_trans FROM trafonet_main.tbl_red_trans WHERE nb_id_red_empre=" + idRed;
                rs = cn.ExecuteQuery(sql);
                trans = new ArrayList();
                while (rs.next()) {
                    ModTransformador tran = new ModTransformador();
                    tran.setIdTrans(rs.getInt("nb_id_trans"));
                    trans.add(tran);
                    tran = null;
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return trans;
    }
    public static ArrayList<ModParametro> cargarAnalisisTiempo() {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        ArrayList<ModParametro> params = null;
        String sql = null;

        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql = " select nb_id_anatime,vc_nombre from trafonet_main.tbl_tiempo_analisis ";
                rs = cn.ExecuteQuery(sql);
                params = new ArrayList();
                while (rs.next()) {
                    ModParametro param = new ModParametro();
                    param.setIdParam(rs.getInt("nb_id_anatime"));
                    param.setNombre(rs.getString("vc_nombre"));
                    params.add(param);
                    param = null;
                }
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return params;
    }
    public static ArrayList cargarDatosVoltaje(String fechaIni, String fechaFin, String tabla, String schema, ModSesion sesion) {
        ResultSet rs = null;
        ConexionSQL cn = new ConexionSQL();
        String sql = null;
        ArrayList datos = null;
        double min = 300;
        double max = 0;
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql =  " select val1,val2,val3,dt_ultlectura ";
                sql += " from "+schema+"."+tabla+"s"; 
                sql += " where dt_ultlectura between '"+fechaIni+"' and '"+fechaFin+"'"; 
                sql += " and val1 <> -1.1"; 
                sql += " order by dt_ultlectura";
                rs = cn.ExecuteQuery(sql);
                datos = new ArrayList();
                while (rs.next()) {
                    ArrayList detalle = new ArrayList();
                    detalle.add(rs.getTimestamp("dt_ultlectura"));
                    detalle.add(rs.getDouble("val1"));
                    detalle.add(rs.getDouble("val2"));
                    detalle.add(rs.getDouble("val3"));
                    datos.add(detalle);
                    if(min > rs.getDouble("val1")){
                        min = rs.getDouble("val1");
                    }else if(min > rs.getDouble("val2")){
                        min = rs.getDouble("val2");
                    }else if(min > rs.getDouble("val3")){
                        min = rs.getDouble("val3");
                    }else{}
                    
                    if(max < rs.getDouble("val1")){
                        max = rs.getDouble("val1");
                    }else if(max < rs.getDouble("val2")){
                        max = rs.getDouble("val2");
                    }else if(max < rs.getDouble("val3")){
                        max = rs.getDouble("val3");
                    }else{}
                }
                sesion.setMin(min-5);
                sesion.setMax(max+5);
                rs.close();
                rs = null;
            }
            cn.closeSQL();
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return datos;
    }
}
