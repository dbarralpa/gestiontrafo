package controllers;

import model.ModPaginacion;

public class PrmPaginacion {

    public ModPaginacion mArtic = new ModPaginacion();
    public static final int LARGODATOS = 3;

//    public void indiceArticulos(ArrayList aux) throws Exception {
//        mArtic = new ModPaginacion();
//        mArtic.datos = new String[LARGODATOS][aux.size()];
//
//        for (int i = 0; i < aux.size(); i++) {
//            ModArticulo articulo = (ModArticulo) aux.get(i);
//            mArtic.datos[0][i] = PrmApp.llenaCaracter(articulo.getCodigo(), 15, '0');
//            mArtic.datos[1][i] = "<br><i>C�digo: </i>" + articulo.getCodigo() + "<br><i>Descripci�n: </i>" + articulo.getDescripcion();
//            mArtic.datos[2][i] = String.valueOf(i);
//            articulo = null;
//        }
//        mArtic.quicksort();
//    }
    public ModPaginacion getDetalle(String id) throws Exception {
        if (id.equals("ARTIC")) {
            return mArtic;
        } else {
            return new ModPaginacion();
        }
    }

    public void setDetalle(String id) throws Exception {
        if (id.equals("ARTIC")) {
            mArtic = new ModPaginacion();
        }
    }

    public String verPaginador(String id, boolean conResumen, boolean ordenAsc) throws Exception {
        String html = "";
        String htm2 = "";
        String tip = "";
        ModPaginacion ver = null;
        ModPaginacion verAux = null;
        int i = 0;
        int tpag = 0;
        int pi = 0;
        int pt = 0;
        int ri = 0;
        int rt = 0;

        verAux = getDetalle(id);
        if (ordenAsc) {
            ver = verAux;
        } else {
            ver = new ModPaginacion(LARGODATOS, verAux.getTotal());
            for (int j = 0; j < verAux.getTotal(); j++) {
                ver.datos[0][j] = verAux.datos[0][verAux.getTotal() - (j + 1)];
                ver.datos[1][j] = verAux.datos[1][verAux.getTotal() - (j + 1)];
                ver.datos[2][j] = verAux.datos[2][verAux.getTotal() - (j + 1)];
            }
            ver.pag = verAux.pag;
        }

        if (ver.getTotal() > 0) {
            tpag = (ver.getTotal() + (ver.RPP - 1)) / ver.RPP;
            if (tpag > ver.TOPE_SET) {
                int y1 = (ver.pag / ver.TOPE_SET);
                y1 = y1 * ver.TOPE_SET;
                if (ver.pag == y1) {
                    pi = ver.pag - (ver.TOPE_SET - 1);
                } else {
                    pi = y1 + 1;
                }

                pt = pi + (ver.TOPE_SET - 1);
                if (pt > tpag) {
                    pt = tpag;
                }
            } else {
                pi = 1;
                pt = tpag;
            }
            ri = (ver.RPP * (ver.pag - 1)) + 1;
            rt = (ver.RPP * ver.pag);
            if (rt > ver.getTotal()) {
                rt = ver.getTotal();
            }

            html = "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
            html = html + "<tr class='Paginador'>\n";
            html = html + "<td width=\"10\">" + "[" + ri + "/" + rt + "]" + "</td>\n";
            html = html + "<td width=\"10\">" + "[" + ver.getTotal() + "]" + "</td>\n";
            if (pi > 1) {
                if (conResumen) {
                    tip = PrmApp.verOverTip("<b>desde: </b>" + ver.codigoDesde_Pag(1, ver.RPP) + "<br>" + "<b>hasta: </b>" + ver.codigoDesde_Pag(pi, ver.RPP));
                } else {
                    tip = "";
                }
                html = html + "<td width=\"17\">";
                html = html + "<input name='pag" + id + "' type='submit' value='�' class='boxNumerada'  " + tip + " onClick=\"this.value='" + (pi - 1) + "'\" >";
                html = html + "</td>\n";
                htm2 = htm2 + "<td>" + "</td>\n";
            }
            for (i = pi; i <= pt; i++) {
                tip = "";
                html = html + "<td width=\"17\">";
                html = html + "<input name='pag" + id + "' type='submit' value='" + i + "' ";
                if (conResumen) {
                    tip = PrmApp.verOverTip("<b>desde: </b>" + ver.codigoDesde_Pag(i, ver.RPP) + "<br>" + "<b>hasta: </b>" + ver.codigoHasta_Pag(i, ver.RPP));
                } else {
                    tip = "";
                }
                if (ver.pag == i) {
                    html = html + "class='boxNumeradaActiva' disabled " + tip + ">";
                } else {
                    html = html + "class='boxNumerada' " + tip + ">";
                }
                html = html + "</td>\n";
            }
            if (tpag > pt) {
                if (conResumen) {
                    tip = PrmApp.verOverTip("<b>desde: </b>" + ver.codigoHasta_Pag(pt, ver.RPP) + "<br>" + "<b>hasta: </b>" + ver.codigoHasta_Pag(tpag, ver.RPP));
                } else {
                    tip = "";
                }
                html = html + "<td width=\"17\">";
                html = html + "<input name='pag" + id + "' type='submit' value='�' class='boxNumerada'  " + tip + " onClick=\"this.value='" + (pt + 1) + "'\" >";
                html = html + "</td>\n";
                htm2 = htm2 + "<td>" + "</td>\n";
            }
            html = html + "<td width=\"600\" align=\"right\">" + "reg. por p�gina:" + "</td>\n";
            html = html + "<td width=\"50\" align='right'>";
            html = html + ver.RPP;
            html = html + "</td>\n";
            html = html + "</tr>\n";
            html = html + "</table>\n";
        } else {
            html = "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
            html = html + "<tr class='Paginador'>\n";
            html = html + "<td width=\"10\">" + "[" + 0 + "/" + 0 + "]" + "</td>\n";
            html = html + "<td width=\"10\">" + "[" + 0 + "]" + "</td>\n";
            html = html + "<td width=\"17\">";
            html = html + "<input name='pag1' type='submit' value='1' class='boxNumeradaActiva' disabled>";
            html = html + "</td>\n";
            html = html + "<td width=\"600\" align=\"right\">" + "</td>\n";
            html = html + "</tr>\n";
            html = html + "</table>\n";
        }
        return html;
    }
}
