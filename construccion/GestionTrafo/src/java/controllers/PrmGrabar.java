package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import model.ModUsuario;
import model.apl.ModEmpresa;
import model.apl.ModEstadistica;
import model.apl.ModParametro;
import sql.ConexionSQL;

public class PrmGrabar {

    public PrmGrabar() {
    }

    public static boolean actualizarEstadoFalla(byte est, byte idFalla, Timestamp fecha, String observacion, String usuario, Timestamp lectura) {
        String vvv = "";
        boolean estado = false;
        boolean estado1 = false;
        ConexionSQL cn = new ConexionSQL();
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                if (est == 3) {
                    vvv += "fallasaldada" + "�";
                    vvv += "FALSE";
                    estado = cn.UpdateReg("trafonet_main.tbl_fallas_trans", "vc_class,bo_estado", vvv, "nb_id_falla_trans =" + idFalla);
                    estado1 = insertarBitacoraFalla(cn, idFalla, fecha, observacion, usuario);
                    cn.commitSQL(estado && estado1);
                } else {
                    vvv += "fallaver";
                    estado = cn.UpdateReg("trafonet_main.tbl_fallas_trans", "vc_class", vvv, "nb_id_falla_trans =" + idFalla);
                    estado1 = insertarBitacoraFalla(cn, idFalla, fecha, observacion, usuario);
                    cn.commitSQL(estado && estado1);
                }
                cn.closeSQL();
            }
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return estado;
    }

    private static boolean insertarBitacoraFalla(ConexionSQL cn, byte idFalla, Timestamp fecha, String observacion, String usuario) {
        String vvv = "";
        boolean estado = false;
        try {
            if (idFalla > 0 && !observacion.trim().equals("") && !usuario.trim().equals("")) {
                vvv += idFalla + "�";
                vvv += fecha + "�";
                vvv += observacion + "�";
                vvv += usuario;
                estado = cn.InsertReg("trafonet_main.tbl_bitacora_fallas", "nb_id_fallas_trans, dt_fecha, vc_observacion, vc_usuario", vvv);
            }
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return estado;
    }

    public static boolean actualizarMaxVariable(int trans, int empre, ArrayList<ModEstadistica> estadis) {
        boolean estado = false;
        ConexionSQL cn = new ConexionSQL();
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                String vvv = null;
                Timestamp fecha = new Timestamp(new Date().getTime());
                for (int i = 0; i < estadis.size(); i++) {
                    vvv = null;
                    ModEstadistica esta = (ModEstadistica) estadis.get(i);
                    if (esta.getMaxLim() > 0) {
                        vvv = 210 + "�";
                        vvv += esta.getMaxLim() + "�";
                        vvv += 0 + "�";
                        vvv += 0 + "�";
                        vvv += esta.getIdEsta() + "�";
                        vvv += fecha + "�";
                        vvv += 0 + "�";
                        vvv += 0 + "�";
                        vvv += true + "�";
                        vvv += true + "�";
                        vvv += trans + "�";
                        vvv += empre;
                        estado = cn.UpdateReg("trafonet_main.tbl_limites_variable_empresa", "nb_max_lim", String.valueOf(esta.getMaxLim()), "nb_id_trans=" + trans + " and nb_id_empresa=" + empre + " AND nb_id_var_lim=" + esta.getIdEsta());
                        if (!estado) {
                            estado = cn.InsertReg("trafonet_main.tbl_limites_variable_empresa", "nb_min_lim, nb_max_lim, nb_desv_lim, nb_promedio_lim, nb_id_var_lim, "
                                    + "nb_ultimafecha_lim, nb_factorcarga_lim, nb_cant_lim, bo_cal_fac_car_lim, "
                                    + "bo_cal_desv_lim, nb_id_trans, nb_id_empresa", vvv);
                        }
                        if (!estado) {
                            break;
                        }
                    }
                    esta = null;
                }
                cn.commitSQL(estado);
                cn.closeSQL();

            }


        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return estado;
    }

    public static boolean actualizarParametros(String valor, double val, ModUsuario usu, Timestamp fec) {
        String sql = "";
        String vvv = "";
        String campos = "";
        ResultSet rs = null;
        boolean estado = false;
        byte count = 0;
        ConexionSQL cn = new ConexionSQL();
        try {
            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
                sql = " SELECT COUNT(*) AS count FROM trafonet_main.tbl_parametros WHERE";
                if (valor.equals("trafonet")) {
                    sql += " nb_id_parametros_trafonet = " + usu.getEmpresa().getParametro().getIdParam() + " AND nb_id_trans = -1 AND nb_id_empre = -1 ";
                } else if (valor.equals("empresa")) {
                    sql += " nb_id_parametros_trafonet = " + usu.getEmpresa().getParametro().getIdParam() + " AND nb_id_trans = -1 AND nb_id_empre = " + usu.getEmpresa().getIdEmpre() + " ";
                } else if (valor.equals("equipo")) {
                    sql += " nb_id_parametros_trafonet = " + usu.getEmpresa().getParametro().getIdParam() + " AND nb_id_trans = " + usu.getEmpresa().getTransformador().getIdTrans() + " AND nb_id_empre = " + usu.getEmpresa().getIdEmpre() + " ";
                }
                rs = cn.ExecuteQuery(sql);
                while (rs.next()) {
                    count = rs.getByte("count");
                }
                rs.close();
                rs = null;
                if (count > 0) {
                    vvv += val;
                    if (valor.equals("trafonet")) {
                        estado = cn.UpdateReg("trafonet_main.tbl_parametros", "nb_valor", vvv, "nb_id_parametros_trafonet=" + usu.getEmpresa().getParametro().getIdParam() + " "
                                + "AND nb_id_trans = -1 AND nb_id_empre = -1");
                    } else if (valor.equals("empresa")) {
                        estado = cn.UpdateReg("trafonet_main.tbl_parametros", "nb_valor", vvv, "nb_id_parametros_trafonet=" + usu.getEmpresa().getParametro().getIdParam() + " "
                                + "AND nb_id_trans = -1 AND nb_id_empre = " + usu.getEmpresa().getIdEmpre() + " ");
                    } else if (valor.equals("equipo")) {
                        estado = cn.UpdateReg("trafonet_main.tbl_parametros", "nb_valor", vvv, "nb_id_parametros_trafonet=" + usu.getEmpresa().getParametro().getIdParam() + " "
                                + "AND nb_id_trans = " + usu.getEmpresa().getTransformador().getIdTrans() + " AND nb_id_empre = " + usu.getEmpresa().getIdEmpre() + " ");
                    }

                    if (estado) {
                        campos = "nb_id_parametros,dt_fecha,vc_usuario,vc_area";
                        vvv = "";
                        vvv += usu.getEmpresa().getParametro().getIdParam() + "�";
                        vvv += new Timestamp(new java.util.Date().getTime()) + "�";
                        vvv += usu.getUser() + "�";
                        vvv += valor;

                        estado = cn.InsertReg("trafonet_main.tbl_bitacora_parametros", campos, vvv);
                    }
                } else {
                    sql = " SELECT MAX(nb_id_param_empre)+1 AS maximo FROM trafonet_main.tbl_parametros ";
                    rs = cn.ExecuteQuery(sql);
                    while (rs.next()) {
                        count = rs.getByte("maximo");
                    }
                    rs.close();
                    rs = null;
                    campos = "nb_id_param_empre,nb_id_parametros_trafonet,nb_id_trans,nb_id_empre,nb_valor";
                    vvv = "";
                    vvv += count + "�";
                    vvv += usu.getEmpresa().getParametro().getIdParam() + "�";
                    if (valor.equals("trafonet")) {
                        vvv += -1 + "�";
                        vvv += -1 + "�";
                        vvv += val;
                        estado = cn.InsertReg("trafonet_main.tbl_parametros", campos, vvv);
                    } else if (valor.equals("empresa")) {
                        vvv += -1 + "�";
                        vvv += usu.getEmpresa().getIdEmpre() + "�";
                        vvv += val;
                        estado = cn.InsertReg("trafonet_main.tbl_parametros", campos, vvv);
                    } else if (valor.equals("equipo")) {
                        vvv += usu.getEmpresa().getTransformador().getIdTrans() + "�";
                        vvv += usu.getEmpresa().getIdEmpre() + "�";
                        vvv += val;
                        estado = cn.InsertReg("trafonet_main.tbl_parametros", campos, vvv);
                    }
                    if (estado) {
                        campos = "nb_id_parametros,dt_fecha,vc_usuario,vc_area";
                        vvv = "";
                        vvv += count + "�";
                        vvv += fec + "�";
                        vvv += usu.getUser() + "�";
                        vvv += valor;
                        estado = cn.InsertReg("trafonet_main.tbl_bitacora_parametros", campos, vvv);
                    }
                }
                cn.commitSQL(estado);
            }

            cn.closeSQL();
        } catch (SQLException ex) {
            estado = false;
            PrmApp.addError(ex, true);
        }
        return estado;
    }
//    public static boolean actualizarAnalisisVariableFalla1(int trans, int empre, int var, boolean est) {
//        String vvv = "";
//        boolean estado = false;
//        ConexionSQL cn = new ConexionSQL();
//        try {
//            if (cn.openSQL(ConexionSQL.POSTGRES_LOCAL)) {
//                if (est) {
//                    vvv += false;
//                    if (trans == -1) {
//                        //estado = cn.UpdateReg("trafonet_main.tbl_limites_variable_empresa", "bo_analisis", vvv, "nb_id_empresa =" + empre + " AND nb_id_var_lim=" + var);
//                        // if (estado) {
//                        vvv = "";
//                        vvv += false;
//                        estado = cn.UpdateReg("trafonet_main.tbl_limites_variable_empresa", "bo_analisis", vvv, "nb_id_empresa =" + empre + " AND nb_id_var_lim=" + var);
//                        if (estado) {
//                            vvv = "";
//                            vvv += true;
//                            estado = cn.UpdateReg("trafonet_main.tbl_limites_variable_empresa", "bo_analisis", vvv, "nb_id_empresa =" + empre + "AND nb_id_trans=" + trans + " AND nb_id_var_lim=" + var);
//                        }
//                        //}
//                    } else {
//                        vvv = "";
//                        vvv += true;
//                        estado = cn.UpdateReg("trafonet_main.tbl_limites_variable_empresa", "bo_analisis", vvv, "nb_id_empresa =" + empre + " AND nb_id_trans=" + trans + " AND nb_id_var_lim=" + var);
//                    }
//                    cn.commitSQL(estado);
//                } else {
//                    vvv = "";
//                    vvv += false;
//                    estado = cn.UpdateReg("trafonet_main.tbl_limites_variable_empresa", "bo_analisis", vvv, "nb_id_empresa =" + empre + " AND nb_id_var_lim=" + var);
//                    cn.commitSQL(estado);
//                }
//            }
//            cn.closeSQL();
//        } catch (SQLException ex) {
//            PrmApp.addError(ex, true);
//        }
//        return estado;
//    }
}
