/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import model.ModSesion;
import model.apl.ModEstadistica;
import model.apl.ModTransformador;
import model.apl.ModVariable;

/**
 *
 * @author dbarra
 */
public class PrmRegistro {

    public PrmRegistro() {
    }

    public ArrayList abrevVariables() {
        ArrayList abrevVariables = new ArrayList();
        abrevVariables.add("");
        return abrevVariables;
    }

    public static String devolverGrafico(HttpServletRequest req, ModTransformador trans) {
        String html = "";
        String valor = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            ArrayList detalles = trans.getDetalles();
            HashMap hm = new HashMap();
            //  if (Sesion.getUsuario().getEmpresa().getTransformador().getFalla().getIdFalla() == 1) {

            hm.put("val1", "");
            hm.put("val2", "");
            hm.put("val3", "");
            hm.put("val4", "");
            hm.put("val5", "");
            hm.put("val6", "");
            hm.put("val7", "");
            hm.put("val8", "");
            hm.put("val9", "");
            hm.put("val10", "");
            hm.put("val11", "");
            hm.put("val12", "");
            hm.put("val13", "");
            hm.put("val14", "");
            hm.put("val15", "");
            hm.put("val16", "");
            hm.put("val17", "");
            hm.put("val18", "");
            hm.put("val19", "");
            hm.put("val20", "");
            hm.put("val21", "");
            hm.put("val22", "");
            hm.put("val23", "");
            hm.put("val24", "");
            hm.put("val25", "");
            hm.put("val26", "");
            hm.put("val27", "");
            hm.put("val28", "");
            hm.put("val29", "");
            hm.put("val30", "");
            for (int i = 0; i < detalles.size(); i++) {
                for (int a = 0; a < Sesion.getRegSelected().size(); a++) {
                    ModVariable vari = (ModVariable) Sesion.getRegSelected().get(a);
                    if (detalles.size() == 1) {
                        hm.put("val" + (a + 1), hm.get("val" + (a + 1)) + " var val" + (a + 1) + " = [['" + ((ArrayList) detalles.get(i)).get(0) + "'," + ((ArrayList) detalles.get(i)).get(vari.getId() + 1) + "]]" + "\n");
                    } else {
                        if (i == 0) {
                            hm.put("val" + (a + 1), hm.get("val" + (a + 1)) + " var val" + (a + 1) + " = [['" + ((ArrayList) detalles.get(i)).get(0) + "'," + ((ArrayList) detalles.get(i)).get(vari.getId() + 1) + "]," + "\n");

                        } else {
                            if ((detalles.size() - i) > 1) {
                                hm.put("val" + (a + 1), hm.get("val" + (a + 1)) + "['" + ((ArrayList) detalles.get(i)).get(0) + "'," + ((ArrayList) detalles.get(i)).get(vari.getId() + 1) + "]," + "\n");
                            } else {
                                hm.put("val" + (a + 1), hm.get("val" + (a + 1)) + "['" + ((ArrayList) detalles.get(i)).get(0) + "'," + ((ArrayList) detalles.get(i)).get(vari.getId() + 1) + "]];" + "\n");
                            }
                        }
                    }





                }
            }
            // }

            for (int a = 0; a < Sesion.getRegSelected().size(); a++) {
                html += hm.get("val" + (a + 1));
                valor += "val" + (a + 1) + ",";
            }


            // html += "    var goog1 = [[\"2011-05-09 15:13:03.203\",0]];";
            html += " plot = $.jqplot('chart1', [" + valor.substring(0, valor.length() - 1) + "], { " + "\n";
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='5' align='center' class=\"ConBorder\">";
            html += "</td>\n" + "</tr>\n";
            PrmApp.addError(ex, true);
        }
        return html;
    }
    public static String tiempoAnalisis(double tiempo){
        String tiempoAnalisis = "";
        if(tiempo == 7){
            tiempoAnalisis = "Semanal";
        }
        if(tiempo == 30){
            tiempoAnalisis = "Mensual";
        }
        if(tiempo == 365){
            tiempoAnalisis = "Anual";
        }
        return tiempoAnalisis;
    }
    
    public static void cumplimientoVoltaje(ModTransformador trans){
        int val1 = 0;
        int val2 = 0;
        int val3 = 0;
        double minVFA = 0;
        double maxVFA = 0;
        double minVFB = 0;
        double maxVFB = 0;
        double minVFC = 0;
        double maxVFC = 0;
        
        for(int i=0;i<trans.getEstadisticas().size();i++){
            ModEstadistica esta = (ModEstadistica)trans.getEstadisticas().get(i);
            if(esta.getIdEsta() == 0){
               minVFA = esta.getMinLim();
               maxVFA = esta.getMaxLim();
            }
            else if(esta.getIdEsta() == 1){
               minVFB = esta.getMinLim(); 
               maxVFB = esta.getMaxLim();
            }
            else if(esta.getIdEsta() == 2){
               minVFC = esta.getMinLim(); 
               maxVFC = esta.getMaxLim();
            }
            
        }
        for(int i=0;i<trans.getDetalles().size();i++){
            ArrayList detalles = (ArrayList) trans.getDetalles().get(i);
            if((Double)detalles.get(1) < minVFA || (Double)detalles.get(1) > maxVFA){
                val1++;
            } if((Double)detalles.get(2) < minVFB || (Double)detalles.get(2) > maxVFB){
                val2++;
            } if((Double)detalles.get(3) < minVFC || (Double)detalles.get(3) > maxVFC){
                val3++;
            }
        }
        for(int i=0;i<trans.getEstadisticas().size();i++){
            ModEstadistica esta = (ModEstadistica)trans.getEstadisticas().get(i);
            if(esta.getIdEsta() == 0){
               double result = new Double(val1 * 100) / trans.getDetalles().size(); 
               esta.setTiempoCumplimiento(result);                 
            }else if(esta.getIdEsta() == 1){
               double result = new Double(val2 * 100) / trans.getDetalles().size();
               esta.setTiempoCumplimiento(result);                 
            }else if(esta.getIdEsta() == 2){
               double result = new Double(val3 * 100) / trans.getDetalles().size(); 
               esta.setTiempoCumplimiento(result);                 
            }
        }
    }
    public static String[] colorRegistro(){
        String[] colores = new String[30];
        colores[0] = "#000000";
        colores[1] = "#FF0000";
        colores[2] = "#FFFF00";
        colores[3] = "#0000CC";
        return colores;
    }
    
}
