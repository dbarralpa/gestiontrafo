<%@ taglib uri="http://struts.apache.org/tags-bean"  prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<jsp:useBean id="Sesion"        scope="session" class="model.ModSesion"/>
<jsp:useBean id="LOAD_ONCLICK"  scope="request" class="java.lang.String"/>
<jsp:useBean id="vw"        scope="page"        class="view.VwRegistro" />
<%
    response.setHeader("Cache-control", "no-cache");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", 0);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>        
        <title><bean:message key="title.main"/></title>
        <!-- HOJAS DE ESTILO -->
        <link rel="stylesheet" type="text/css" href="css/template.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/table.css" />
        <link rel="stylesheet" href="css/demo-menu-item.css" media="screen" type="text/css" />
        <link rel="stylesheet" href="css/demo-menu-bar.css" media="screen" type="text/css" />
        <link rel="stylesheet" type="text/css" href="css/notification.css" />
        <link rel="stylesheet" type="text/css" href="css/loading.css" />
        <!-- SCRIPT DE APLICACION -->
        <!--script type="text/javascript" src="js/ajax_search.js"></script-->
        <script language="javascript" type="text/javascript" src="js/jquery-1.3.2.js"></script>
        <script language="javascript" type="text/javascript" src="js/overlib.js"></script>
        <script language="javascript" type="text/javascript" src="js/functions.js"></script>
        <script language="javascript" type="text/javascript" src="js/menu-for-applications.js"></script>
        <script language="javascript" type="text/javascript" src="js/jquery.magnifier.js"></script>
        <script language="javascript" type='text/javascript' src='js/jquery.simplemodal.js'></script>
        <script language="javascript" type="text/javascript" src="js/mensajes.js"></script>
        <script language="javascript" type="text/javascript" src="js/loading.js"></script>
        <script language="javascript" type="text/javascript" src="js/popcalendar.js"></script>
        <script language="javascript" type="text/javascript" src="js/initcalendar.js"></script>


    </head>
    <body>
        <div id="overlay" class="v655" style="display:none;"></div>
        <div id="modal" class="v655" style="display:none;">
            <div class="contentLoading"></div>
            <img style="display:none;" src="img/loading16.gif" alt="loading" />
        </div>     
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <div id="app">            
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img alt="" class="mainLogo" src="img/images/bannerGES1.jpg" border="0" /></td>
                </tr>
                <tr>
                    <td>
                        <%try {%>
                        <tiles:insert attribute="menu"/>
                        <%} catch (Exception ex) {
                            vw.addError(ex, true);%>ERROR SECCION<%}
                        %>
                    </td>
                </tr>
                <%if (!Sesion.isEnSesion()) {%>
                <tr>
                    <td>
                        <table align="center" cellpadding="0" cellspacing="0">
                            <tr><td height="100">&nbsp;</td></tr>
                            <tr>
                                <td>
                                    <%try {%>
                                    <tiles:insert attribute="login"/>
                                    <%} catch (Exception ex) {
                                        vw.addError(ex, true);%>ERROR SECCION<%}
                                    %>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%} else {%>
                <tr>
                    <td>
                        <%try {%>
                        <tiles:insert attribute="body"/>
                        <%} catch (Exception ex) {
                            vw.addError(ex, true);%>ERROR SECCION<%}
                        %>
                    </td>
                </tr>
                <%}%>
            </table>
        </div>
    </body>
</html>

<%if (Sesion.isConTip()) {%>
<script type="text/javascript">
    OffMsg("<%=Sesion.getTip()%>", "<%=Sesion.getTipoTip()%>");
</script>
<%Sesion.cleanTip();%>
<%}%>

