function getElemento(obj){
    return document.getElementById(obj);
}
		
function showOverLib(titulo, contenido, fg, bg){
    overlib(contenido, CAPTION, titulo,
        FGCOLOR, fg,
        BGCOLOR, bg,
        BORDER, 1,
        CAPTIONFONT, "Verdana",
        CAPTIONSIZE, 1,
        TEXTFONT,"Verdana",
        TEXTSIZE, 1);
} 				
function changeValue(obj, texto){
    var vItem = getElemento(obj);
    vItem.value=texto;
}

function setFechaHora(obj){
    date = new Date();
    ano = date.getYear();
    mes = date.getMonth()+1;
    dia = date.getDate();
    hora = date.getHours();
    minuto = date.getMinutes();
    segundo = date.getSeconds();    
    if(dia<10){
        dia = "0"+dia;
    }
    if(mes<10){
        mes = "0"+mes;
    }
    if(segundo<10){
        segundo = "0"+segundo;
    }
    if(minuto<10){
        minuto = "0"+minuto;
    }
    dato = ano + "-" + mes + "-" + dia + " " + hora + ":" + minuto + ":" + segundo;
    changeValue(obj, dato);
}
function confirmar(){
    if(confirm('�Seguro que desea proseguir con la operaci�n?')){
    }else{
        return false;
    }
}
