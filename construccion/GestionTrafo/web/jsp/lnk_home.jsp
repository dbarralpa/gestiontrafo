<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<jsp:useBean id="vw"     scope="page"    class="view.VwRegistro" />
<form method="post" action="">
    <input name="frm" type="hidden" value="login" />
    <table width="100%">
        <tr>
            <td height="30" class="colorEnc" colspan="2">DIAGN&Oacute;STICO DE EQUIPOS REMOTOS</td>
        </tr>
        <tr>
            <td width="1030px">
                <table cellpadding="0" cellspacing="0" border="1" width="1013px">
                    <tr>
                        <td class="color" rowspan="2" valign="middle" align="center" width="40px">ID&nbsp;</td>
                        <td class="color" colspan="4" valign="middle" align="center" width="490px">Identificaci&oacute;n&nbsp;</td>
                        <td class="color" rowspan="2" valign="middle" align="center" width="120px">&Uacute;ltima Lectura</td>
                        <td class="color" colspan="6" valign="middle" align="center"  width="315px">Indicadores de uso</td>
                    </tr>
                    <tr>
                        <td class="color"  valign="middle" align="center" width="250px">Descripci&oacute;n</td>
                        <td class="color"  valign="middle" align="center" width="60px">Serie</td>
                        <td class="color"  valign="middle" align="center" width="100px">F&aacute;brica</td>
                        <td class="color"  valign="middle" align="center" width="80px">Fabricaci&oacute;n</td>
                        <td class="color"  valign="middle" align="center" width="80px">Fallas</td>
                        <td class="color"  valign="middle" align="center" width="60px">Factor de Carga(pu)</td>
                        <td class="color"  valign="middle" align="center" width="80px">Factor de demanda(pu)</td>
                        <td class="color"  valign="middle" align="center" width="80px">Factor de utilizaci&oacute;n(pu)</td>
                        <td class="color"  valign="middle" align="center" width="0px">Cortes</td>
                        <!--td class="color"  valign="middle" align="center" width="15px">&nbsp;</td-->
                    </tr>
                </table>
                <div class="ScrollStyle6">
                    <table cellpadding="0" cellspacing="0" border="1" width="1013px">
                        <%=vw.grillaTransformadores(request)%>
                    </table>
                </div>
            </td>
            <td>&nbsp;</td>
            <td valign="top" width="180" align="right">
                <table  border="0">
                    <tr>
                        <td>Seleccionar RED</td>
                    </tr>
                    <tr>
                        <td valign="top" width="180">
                            <table width="180" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td valign="top">
                                        <table border="1" cellpadding="0" cellspacing="0"  width="170px">
                                            <tr>
                                                <td class="color" width="170" align="center">Denominaci&oacute;n</td>
                                                <!--td class="color"  align="center" width="17px">&nbsp;</td-->
                                            </tr>
                                        </table>
                                        <div class="ScrollStyle8">
                                            <table cellpadding="0" cellspacing="0" border="1" width="170px">
                                                <%=vw.cargarRedesEmpresa(request)%>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!--tr>
                        <td align="center"><input type="submit" value="Editar REDES" class="boton"></td>
                    </tr-->
                </table>
            </td>
        </tr>
    </table>
</form>